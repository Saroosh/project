from rest_framework import serializers
from .models import *


class lectureSerializer(serializers.ModelSerializer):
    class Meta:
        model = LectureModel
        fields = '__all__'

class classSerializer(serializers.ModelSerializer):
    class Meta:
        model = TblClassModel
        fields = ('__all__')

class calenderSerializer(serializers.ModelSerializer):
    class Meta:
        model = CalenderModel
        fields = '__all__'


class StudentFeedbackSerializer(serializers.ModelSerializer):
    class Meta:
        model = StudentFeedBackModel
        fields = '__all__'

class DiscussionSerializer(serializers.ModelSerializer):
    class Meta:
        model = DiscussionModel
        fields = '__all__'

class createquizSerializer(serializers.ModelSerializer):
    # course = serializers.StringRelatedField(many=False)
    # classes = serializers.StringRelatedField(many=False)
    class Meta:
        model = createQuizModel
        fields = '__all__'


class questionSerializer(serializers.ModelSerializer):
    class Meta:
        model = quizQuestionsAndAnswersModel
        fields =  '__all__'
        
class courseSerializer(serializers.ModelSerializer):
    class Meta:
        model = CourseModel
        fields = '__all__' 
        
class schoolSerializer(serializers.ModelSerializer):
    class Meta:
        model = SchoolModel
        fields = '__all__' 

class teacherSerializer(serializers.ModelSerializer):
    class Meta:
        model = TeacherModel
        fields = '__all__'
        
class assigmentSerializer(serializers.ModelSerializer):
    classes = serializers.StringRelatedField(many=False)
    course = serializers.StringRelatedField(many=False)
    school = serializers.StringRelatedField(many=False)
    teacher = serializers.StringRelatedField(many=False)

    class Meta:
        model = AssignmentModel
        fields = '__all__'


class testSerializer(serializers.ModelSerializer):
    classes = serializers.StringRelatedField(many=False)
    course = serializers.StringRelatedField(many=False)
    school = serializers.StringRelatedField(many=False)
    teacher = serializers.StringRelatedField(many=False)

    class Meta:
        model = TestModel
        fields = '__all__'
class resultSerializer(serializers.ModelSerializer):
    class Meta:
        model = ResultModel
        fields = '__all__'

class submittedAssignmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = SubmitAssignmentModel
        fields = '__all__'

