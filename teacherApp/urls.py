
from django.contrib import admin
from django.urls import path
from teacherApp.views import *
from teacherApp import views

from django.conf.urls import url, include
from django.conf import settings
from django.conf.urls.static import static
from django.views.static import serve
from rest_framework.urlpatterns import format_suffix_patterns


urlpatterns = [

    path('assignment', views.AddAssignment.as_view() , name = "addAssignmnet"),
    path('detail_assignment/<str:slug>/', views.DetailAssignment.as_view(), name = "deleteAssignmnet"),

    path('quiz', views.Quiz.as_view(), name = "quiz"),
    path('quiz_detail/<str:slug>/', views.QuizDetail.as_view(), name = "detail_quiz"),

    path('lecture', views.AddLecture.as_view(), name = "lecture"),
    path('lecture_detail/<str:slug>/', views.LectureDetail.as_view(), name = "lecture_detail"),

    path('feedback', views.Feedback.as_view(), name = "feedback"),
    path('feedback_detail/<int:pk>/', views.FeedbackDetail.as_view(), name = "feedback_detail"),

    path('discussion', views.Discussion.as_view(), name = "discussion"),
    path('discussion_detail/<str:slug>/', views.DiscussionDetail.as_view(), name = "discussion_detail"),

    path('calender', views.AddCalender.as_view(), name='calender'),
    path('calender_detail/<str:slug>/', views.CalenderDetail.as_view(), name='del_cal'),

    path('article', views.ArticlesView.as_view(), name='article'),
    path('article_detail/<str:slug>/', views.DetailArticlesView.as_view(), name='article_detail'),

    path('onlinequiz', views.AddQuestionView.as_view(), name='onlinequiz'),
    path('onlinequiz_detail/<str:slug>/', views.DetailQuestionView.as_view(), name='onlinequiz_detail'),  

    path('test', views.TestView.as_view(), name='test'),
    path('test_detail/<str:slug>/', views.DetailTestView.as_view(), name='test_detail'), 

    path('result', views.ResultView.as_view(), name='result'),
    path('result_detail/<int:pk>/', views.DetailResultView.as_view(), name='result_detail'), 

    path('submitted_assignment' , views.SubmittedAssignmentView.as_view(), name='submitted_assignment'),
]

urlpatterns = format_suffix_patterns(urlpatterns)

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)