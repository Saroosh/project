
# Create your views here.
from django.shortcuts import render
from django.contrib.auth.models import User
from django.contrib.auth import logout, authenticate, login
from django.forms.formsets import formset_factory
from django.utils.timezone import datetime
from adminapp.models import *

# ***************** API ****************
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser
from .models import *
from django.http import Http404
from .serializers import *
from rest_framework.views import APIView
from rest_framework.parsers import JSONParser,FileUploadParser,MultiPartParser,FormParser
from rest_framework.response import Response
from rest_framework import status,viewsets
from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import IsAuthenticated,IsAuthenticatedOrReadOnly
from superadmin.custompermissions import *
from superadmin.serializers import *
from superadmin.models import ArticleModel


# Create your views here.

class AddAssignment(APIView):

    #permission_classes = (IsAuthenticated,SuperAdminOrTeacherPermission)
    #authentication_classes = (SessionAuthentication,)
    parser_classes = (MultiPartParser,FormParser)

    def get(self,request):
        assignment = AssignmentModel.objects.all()
        serializer = assigmentSerializer(assignment, many = True)
        return Response(serializer.data,status = status.HTTP_200_OK)

    def post(self,request,format = None):
        data = JSONParser().parse(self.request)
        serializer = assigmentSerializer(data = data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status = status.HTTP_201_CREATED)
        else:
            return Response(serializer.data,status = status.HTTP_400_BAD_REQUEST)


class DetailAssignment(APIView):
    #permission_classes = (IsAuthenticated,SuperAdminOrTeacherPermission)
    parser_classes = (MultiPartParser,FormParser)

    def get_object(self, slug):
        try:
            return AssignmentModel.objects.get(slug=slug)
        except AssignmentModel.DoesNotExist:
            raise Http404

    def get(self, request, slug, format=None):
        data = self.get_object(slug)
        serilizer = assigmentSerializer(data)
        return Response(serilizer.data)

    def delete(self, request, slug, format=None):
        assignement = self.get_object(slug)
        assignement.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class Quiz(APIView):
    permission_classes = (IsAuthenticated,SuperAdminOrTeacherPermission)
    authentication_classes = (SessionAuthentication,)
    parser_classes = (JSONParser,)
    def get(self,request):
        quiz = createQuizModel.objects.all()
        serializer = createquizSerializer(quiz, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self,request, format = None):
        # data = JSONParser().parse(request)
        serializer = createquizSerializer(data = request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status=status.HTTP_201_CREATED)
        else:
            serializer = createquizSerializer()
            return Response(serializer.errors,status = status.HTTP_406_NOT_ACCEPTABLE)

class QuizDetail(APIView):
    permission_classes = (IsAuthenticated,SuperAdminOrTeacherPermission)
    authentication_classes = (SessionAuthentication,)
    def get_object(self,slug):
        try:
            return createQuizModel.objects.get(slug=slug)
        except:
            raise Http404

    def get(self,request,slug):
        data = self.get_object(slug)
        serializer = createquizSerializer(data)
        return Response(serializer.data)

    def put(self,request,slug):
        data = self.get_object(slug)
        serializer = createquizSerializer(data = request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    def delete(self,request,slug):
        data = self.get_object(slug)
        data.delete()
        return Response(status = status.HTTP_204_NO_CONTENT)


class AddLecture(APIView):
    # permission_classes = (IsAuthenticated,SuperAdminOrTeacherPermission)
    # authentication_classes = (SessionAuthentication,)
    def get(self,request,format=None):
        data = LectureModel.objects.all()
        serializer = lectureSerializer(data, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self,request,format=None):
        data = JSONParser().parse(request)
        serializer = lectureSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status =status.HTTP_201_CREATED)
        else:
            serializer = lectureSerializer()
            return Response(serializer.errors,status =status.HTTP_400_BAD_REQUEST)

class LectureDetail(APIView):
    permission_classes = (IsAuthenticated,SuperAdminOrTeacherPermission)
    authentication_classes = (SessionAuthentication,)
    def get_object(self,slug):
        try:
            return LectureModel.objects.get(slug=slug)
        except:
            raise Http404

    def get(self,request,slug,format=None):
        data = self.get_object(slug)
        serializer = lectureSerializer(data)
        return Response(serializer.data)

    def put(self,request,slug,format=None):
        data = self.get_object(slug)
        serializer = lectureSerializer(data, data = request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    def delete(self,request,slug,format=None):
        data = self.get_object(slug)
        data.delete()
        return Response(status = status.HTTP_204_NO_CONTENT)

class Feedback(APIView):
    # permission_classes = (IsAuthenticated,SuperAdminOrTeacherPermission)
    # authentication_classes = (SessionAuthentication,)
    parser_classes = (JSONParser,)

    def get(self,request):
        data = StudentFeedBackModel.objects.all()
        serializer = StudentFeedbackSerializer(data, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self,request):
        # data = JSONParser().parse(request)
        # print(data)
        serializer = StudentFeedbackSerializer(data = request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status =status.HTTP_201_CREATED)
        else:
            serializer = StudentFeedbackSerializer()
            return Response(serializer.errors,status =status.HTTP_400_BAD_REQUEST)

class FeedbackDetail(APIView):
    # permission_classes = (IsAuthenticated,SuperAdminOrTeacherPermission)
    # authentication_classes = (SessionAuthentication,)
    def get_object(self,pk):
        try:
            return StudentFeedBackModel.objects.get(id=pk)
        except:
            raise Http404

    def get(self,request,pk):
        data = self.get_object(pk)
        serializer = StudentFeedbackSerializer(data)
        return Response(serializer.data)

    def delete(self,request,pk):
        data = self.get_object(pk)
        data.delete()
        return Response(status = status.HTTP_204_NO_CONTENT)

class Discussion(APIView):
    # permission_classes = (IsAuthenticated,SuperAdminOrTeacherPermission)
    # authentication_classes = (SessionAuthentication,)
    parser_classes = (JSONParser,)
    def get(self,request):
        data = DiscussionModel.objects.all()
        serializer = DiscussionSerializer(data, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self,request):
        # data = JSONParser().parse(request)
        serializer = DiscussionSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status =status.HTTP_201_CREATED)
        else:
            serializer = DiscussionSerializer()
            return Response(serializer.errors,status =status.HTTP_400_BAD_REQUEST)

class DiscussionDetail(APIView):
    # permission_classes = (IsAuthenticated,SuperAdminOrTeacherPermission)
    # authentication_classes = (SessionAuthentication,)
    def get_object(self,slug):
        try:
            return DiscussionModel.objects.get(slug=slug)
        except:
            raise Http404

    def get(self,request,slug):
        data = self.get_object(slug)
        serializer = DiscussionSerializer(data)
        return Response(serializer.data)

    def delete(self,request,slug):
        data = self.get_object(slug)
        data.delete()
        return Response(status = status.HTTP_204_NO_CONTENT)

class AddCalender(APIView):
    # permission_classes = (IsAuthenticated,SuperAdminOrTeacherPermission)
    # authentication_classes = (SessionAuthentication,)
    parser_classes = (JSONParser,)

    def get(self,request,format=None):
        data = CalenderModel.objects.all()
        serializer = calenderSerializer(data, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self,request,format=None):
        # data = JSONParser().parse(request)
        serializer = calenderSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status =status.HTTP_201_CREATED)
        else:
            serializer = calenderSerializer()
            return Response(serializer.errors,status =status.HTTP_400_BAD_REQUEST)

class CalenderDetail(APIView):
    # permission_classes = (IsAuthenticated,SuperAdminOrTeacherPermission)
    # authentication_classes = (SessionAuthentication,)
    def get_object(self,slug):
        try:
            return CalenderModel.objects.get(slug=slug)
        except:
            raise Http404

    def get(self,request,slug,format=None):
        data = self.get_object(slug)
        serializer = calenderSerializer(data)
        return Response(serializer.data)

    def put(self,request,slug,format=None):
        data = self.get_object(slug)
        serializer = calenderSerializer(data,data = request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    def delete(self,request,slug,format=None):
        data = self.get_object(slug)
        data.delete()
        return Response(status = status.HTTP_204_NO_CONTENT)

class ArticlesView(APIView):
    # parser_classes = (MultiPartParser,FormParser)
    parser_classes = (JSONParser,)
    def get(self,request,format = None):
        data = ArticleModel.objects.all()
        serializer = ArticleSerializer(data, many = True)
        return Response(serializer.data,status = status.HTTP_200_OK)

    def post(self,request, format=None):
        serializer = ArticleSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status = status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors,status = status.HTTP_400_BAD_REQUEST)


    def perform_create(self, serializer):
        serializer.save(User=self.request.user)


class DetailArticlesView(APIView):
    parser_classes = (MultiPartParser,FormParser)
    permission_classes = [IsAuthenticatedOrReadOnly]

    def get_object(self, slug):
        try:
            return ArticleModel.objects.get(slug=slug)
        except ArticleModel.DoesNotExist:
            raise Http404

    def get(self, request, slug, format=None):
        data = self.get_object(slug)
        serilizer = ArticleSerializer(data)
        return Response(serilizer.data)

    def put(self,request,slug):
        data = self.get_object(slug)
        serializer = ArticleSerializer(data , data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, slug, format=None):
        data = self.get_object(slug)
        data.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class AddQuestionView(APIView):
    parser_classes = (JSONParser,)
    def get(self,request):
        data = quizQuestionsAndAnswersModel.objects.all()
        serializer = questionSerializer(data, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self,request):
        # data = JSONParser().parse(request)
        serializer = questionSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status =status.HTTP_201_CREATED)
        else:
            serializer = questionSerializer()
            return Response(serializer.errors,status =status.HTTP_400_BAD_REQUEST)

class DetailQuestionView(APIView):
    def get_object(self,slug):
        try:
            return quizQuestionsAndAnswersModel.objects.get(slug=slug)
        except:
            raise Http404

    def get(self,request,slug,format=None):
        data = self.get_object(slug)
        serializer = questionSerializer(data)
        return Response(serializer.data)

    def put(self,request,slug,format=None):
        data = self.get_object(slug)
        serializer = questionSerializer(data,data = request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    def delete(self,request,slug,format=None):
        data = self.get_object(slug)
        data.delete()
        return Response(status = status.HTTP_204_NO_CONTENT)

class TestView(APIView):
    parser_classes = (JSONParser,)

    def get(self,request):
        data = TestModel.objects.all()
        serializer = testSerializer(data, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self,request):
        
        # data = JSONParser().parse(request)
        serializer = testSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status =status.HTTP_201_CREATED)
        else:
            serializer = OnlineTestSerializer()
            return Response(serializer.errors,status =status.HTTP_400_BAD_REQUEST)

class DetailTestView(APIView):
    def get_object(self,slug):
        try:
            return TestModel.objects.get(slug=slug)
        except:
            raise Http404

    def get(self,request,slug,format=None):
        data = self.get_object(slug)
        serializer = testSerializer(data)
        return Response(serializer.data)

    def put(self,request,slug,format=None):
        data = self.get_object(slug)
        serializer = testSerializer(data = request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    def delete(self,request,slug,format=None):
        data = self.get_object(slug)
        data.delete()
        return Response(status = status.HTTP_204_NO_CONTENT)

class ResultView(APIView):
    parser_classes = (JSONParser,)
    def get(self,request):
        data = ResultModel.objects.all()
        serializer = resultSerializer(data, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self,request):
        # data = JSONParser().parse(request)
        serializer = resultSerializer(data = request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status =status.HTTP_201_CREATED)
        else:
            serializer = resultSerializer()
            return Response(serializer.errors,status =status.HTTP_400_BAD_REQUEST)

class DetailResultView(APIView):
    def get_object(self,pk):
        try:
            return ResultModel.objects.get(id=pk)
        except:
            raise Http404

    def get(self,request,pk,format=None):
        data = self.get_object(pk)
        serializer = resultSerializer(data)
        return Response(serializer.data)

    def put(self,request,pk,format=None):
        data = self.get_object(pk)
        serializer = resultSerializer(data,data = request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    def delete(self,request,pk,format=None):
        data = self.get_object(pk)
        data.delete()
        return Response(status = status.HTTP_204_NO_CONTENT)

class SubmittedAssignmentView(APIView):
    def get(self,request):
        data = SubmitAssignmentModel.objects.all()
        serializer = submittedAssignmentSerializer(data, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

#################################################


'''
***************************************************************************
|               Documentation
***************************************************************************

1)Attendance:  Attendance will taken when Student will active in class by
his/her personal student account

2) Login/Registration/forgot/logout: For Login and Registration we will
use Django built-in model and create its relation with role model and then redirect it to dashboard
corresponding his/her role

'''
