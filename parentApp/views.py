from django.shortcuts import render
from superadmin.models import ComplaintModel,CourseModel,  ScheduleModel, AnnouncementModel, AttendanceModel
from superadmin.serializers import  ComplaintSerializer,CourseSerializer, ScheduleSerializer, AnnouncementSerializer, AttendanceSerializer
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status,viewsets,permissions
from rest_framework.parsers import JSONParser,FileUploadParser,MultiPartParser,FormParser
from rest_framework import generics
from rest_framework.parsers import JSONParser
from django.http import JsonResponse
from django.http import Http404
from django.views.decorators.csrf import csrf_exempt
from adminapp.models import *

# Create your views here.
class CoursesView(APIView):
    def get(self,request,format = None):
        data = CourseModel.objects.all()
        serializer = CourseSerializer (data, many = True)
        return Response(serializer.data,status = status.HTTP_200_OK)

    def post(self,request, format=None):
        serializer = CourseSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status = status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors,status = status.HTTP_400_BAD_REQUEST)

class DetailCoursesView(APIView):
    parser_classes = (MultiPartParser,FormParser)


    def get_object(self, pk):
        try:
            return CourseModel.objects.get(id=pk)
        except CourseModel.DoesNotExist:
            raise Http404


    def get(self, request, pk, format=None):
        data = self.get_object(pk)
        serilizer = CourseSerializer(data)
        return Response(serilizer.data)

    def put(self,request,pk):
        data = self.get_object(pk)
        serializer = CourseSerializer(data , data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        data = self.get_object(pk)
        data.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class ComplaintView(APIView):
    def get(self,request,format = None):
        data = ComplaintModel.objects.all()
        serializer = ComplaintSerializer (data, many = True)
        return Response(serializer.data,status = status.HTTP_200_OK)

    def post(self,request, format=None):
        serializer = ComplaintSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status = status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors,status = status.HTTP_400_BAD_REQUEST)

class DetailComplaintView(APIView):
    parser_classes = (MultiPartParser,FormParser)


    def get_object(self, pk):
        try:
            return ComplaintModel.objects.get(id=pk)
        except ComplaintModel.DoesNotExist:
            raise Http404


    def get(self, request, pk, format=None):
        data = self.get_object(pk)
        serilizer = ComplaintSerializer(data)
        return Response(serilizer.data)

    def put(self,request,pk):
        data = self.get_object(pk)
        serializer = ComplaintSerializer(data , data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        data = self.get_object(pk)
        data.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class ScheduleView(APIView):
    def get(self,request,format = None):
        data = ScheduleModel.objects.all()
        serializer = ScheduleSerializer (data, many = True)
        return Response(serializer.data,status = status.HTTP_200_OK)

    def post(self,request, format=None):
        serializer = ScheduleSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status = status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors,status = status.HTTP_400_BAD_REQUEST)

class DetailScheduleView(APIView):
    parser_classes = (MultiPartParser,FormParser)


    def get_object(self, pk):
        try:
            return ScheduleModel.objects.get(id=pk)
        except ScheduleModel.DoesNotExist:
            raise Http404


    def get(self, request, pk, format=None):
        data = self.get_object(pk)
        serilizer = ScheduleSerializer(data)
        return Response(serilizer.data)

    def put(self,request,pk):
        data = self.get_object(pk)
        serializer = ScheduleSerializer(data , data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        data = self.get_object(pk)
        data.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class AnnouncementView(APIView):
    parser_classes = (MultiPartParser,FormParser)

    def get(self,request,format = None):
        data = AnnouncementModel.objects.all()
        serializer = AnnouncementSerializer (data, many = True)
        return Response(serializer.data,status = status.HTTP_200_OK)

    def post(self,request, format=None):
        serializer = AnnouncementSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status = status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors,status = status.HTTP_400_BAD_REQUEST)

class DetailAnnouncementView(APIView):
    parser_classes = (MultiPartParser,FormParser)


    def get_object(self, pk):
        try:
            return AnnouncementModel.objects.get(id=pk)
        except AnnouncementModel.DoesNotExist:
            raise Http404


    def get(self, request, pk, format=None):
        data = self.get_object(pk)
        serilizer = AnnouncementSerializer(data)
        return Response(serilizer.data)

    def put(self,request,pk):
        data = self.get_object(pk)
        serializer = AnnouncementSerializer(data , data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        data = self.get_object(pk)
        data.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class AttendanceView(APIView):
    def get(self,request,format = None):
        data = AttendanceModel.objects.all()
        serializer = AttendanceSerializer (data, many = True)
        return Response(serializer.data,status = status.HTTP_200_OK)

    def post(self,request, format=None):
        serializer = AttendanceSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status = status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors,status = status.HTTP_400_BAD_REQUEST)

class DetailAttendanceView(APIView):
    parser_classes = (MultiPartParser,FormParser)


    def get_object(self, pk):
        try:
            return AttendanceModel.objects.get(id=pk)
        except AttendanceModel.DoesNotExist:
            raise Http404


    def get(self, request, pk, format=None):
        data = self.get_object(pk)
        serilizer = AttendanceSerializer(data)
        return Response(serilizer.data)

    def put(self,request,pk):
        data = self.get_object(pk)
        serializer = AttendanceSerializer(data , data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        data = self.get_object(pk)
        data.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
