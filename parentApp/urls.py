from django.contrib import admin
from django.urls import path
from parentApp.views import *
from parentApp import views

from django.conf.urls import url, include

urlpatterns = [
    path('course', views.CoursesView.as_view(), name = "course"),
    path('course_detail/<int:pk>/', views.DetailCoursesView.as_view(), name = "course"),
    path('complaint', views.ComplaintView.as_view(), name = "complaint"),
    path('complaint_detail/<int:pk>/', views.DetailComplaintView.as_view(), name = "complaint"),
    path('schedule', views.ScheduleView.as_view(), name = "schedule"),
    path('schedule_detail/<int:pk>/', views.DetailScheduleView.as_view(), name = "schedule"),
    path('announcement', views.AnnouncementView.as_view(), name = "announcement"),
    path('announcement_detail/<int:pk>/', views.DetailAnnouncementView.as_view(), name = "announcement"),
    path('attendance', views.AttendanceView.as_view(), name = "attendance"),
    path('attendance_detail/<int:pk>/', views.DetailAttendanceView.as_view(), name = "attendance"),




]
