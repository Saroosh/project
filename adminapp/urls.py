from django.contrib import admin
from django.urls import path
from adminapp.views import *
from adminapp import views

from django.conf.urls import url, include
from django.conf import settings
from django.conf.urls.static import static
from django.views.static import serve
from rest_framework.urlpatterns import format_suffix_patterns

urlpatterns = [
    
    # path('',views.dashboard , name = 'dashboard'),
    path('Add_Teacher', views.Add_Teacher.as_view() , name = "Add_Teacher"),
    path('Add_Student', views.Add_Student.as_view() , name = "Add_Student"),
    path('Assign_Course', views.Assign_Course.as_view() , name = "Assign_Course"),
    path('Enroll_Student', views.Enroll_Student.as_view() , name = "'Enroll_Student'"),
    path('Add_Course', views.Add_Course.as_view() , name = "Add_Course"),
    path('View_Course', views.View_Course.as_view() , name = "View_Course"),
    path('Edit_Course/<str:slug>/', views.Edit_Course.as_view() , name = "Edit_Course"),
    path('Delete_Course/<str:slug>/', views.Delete_Course.as_view() , name = "Delete_Course"),
    path('Unassign_Course', views.Unassign_Course.as_view() , name = "Unassign_Course"),
    path('Add_Class', views.Add_Class.as_view() , name = "Add_Class"),
    path('View_Class', views.View_Class.as_view() , name = "View_Class"),
    path('Edit_Class/<str:slug>/', views.Edit_Class.as_view() , name = "Edit_Class"),
    path('Delete_Class/<str:slug>/', views.Delete_Class.as_view() , name = "Delete_Class"),
    path('Modify_Teacher/<str:slug>/' , views.Modify_Teacher.as_view() , name = 'Modify_Teacher'),
    path('Modify_Student/<str:slug>/' , views.Modify_Student.as_view() , name = 'Modify_Student'),
    path('Delete_Teacher/<str:slug>/' , views.Delete_Teacher.as_view() , name = 'Delete_Teacher'),
    path('Delete_Student/<str:slug>/' , views.Delete_Student.as_view() , name = 'Delete_Student'),
    path('feedback' , views.feedback.as_view() , name = 'feedback'),
    path('View_Queries', views.View_Queries.as_view(), name = 'View_Queries'),
    path('Respond_Queries/<int:pk>/', views.Respond_Queries.as_view(), name = 'Respond_Queries'),
    ]

urlpatterns = format_suffix_patterns(urlpatterns)