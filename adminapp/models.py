
import random
from django.conf import settings
from django.db import models
from django.contrib.auth import get_user_model
from django.template.defaultfilters import slugify

# Create your models here.

class SchoolModel(models.Model):
    title = models.CharField(max_length=50)
    image = models.ImageField(default='default.jpg')
    address = models.CharField(max_length=500)
    email = models.EmailField()
    password = models.CharField(max_length=13)
    contact_no = models.CharField(max_length=12)
    created_at = models.DateTimeField (auto_now=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title

class TblClassModel(models.Model):
    title = models.CharField(max_length=50)
    school = models.ForeignKey(SchoolModel, on_delete=models.CASCADE, default=0)
    slug = models.SlugField(max_length = 250,unique=True, null = True, blank = True) 
    created_at = models.DateTimeField (auto_now=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        r = random.randint(0,100)
        self.slug = slugify(self.title) + '_' + str(r)
        super(TblClassModel, self).save(*args, **kwargs)

class CourseModel(models.Model):
    title = models.CharField(max_length=50)
    description = models.CharField(max_length=500)
    code = models.CharField(max_length=50)
    video_link = models.FileField(default='default_link')
    image_link = models.URLField(default='default_link')
    long_description = models.TextField()
    course_type = models.CharField(max_length=50)
    classes = models.ForeignKey(TblClassModel, on_delete=models.CASCADE)
    slug = models.SlugField(max_length = 250,unique=True, null = True, blank = True) 
    created_at = models.DateTimeField (auto_now=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        r = random.randint(0,100)
        self.slug = slugify(self.title) + '_' + str(r)
        super(CourseModel, self).save(*args, **kwargs)

class TeacherModel(models.Model):
    title = models.CharField(max_length=50)
    address = models.CharField(max_length=200)
    email = models.EmailField()
    #school = models.ForeignKey(SchoolModel, on_delete=models.CASCADE)
    passowrd = models.CharField(max_length=13)
    image = models.ImageField(default='default.jpg')
    reg_no = models.CharField(max_length=20)
    slug = models.SlugField(max_length = 250,unique=True, null = True, blank = True) 
    created_at = models.DateTimeField (auto_now=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        r = random.randint(0,100)
        self.slug = slugify(self.title) + '_' + str(r)
        super(TeacherModel, self).save(*args, **kwargs)

class StudentModel(models.Model):
    
    title = models.CharField(max_length=50)
    reg_no = models.CharField(max_length=20)
    email = models.EmailField(max_length=50)
    address = models.CharField(max_length=200)
    school = models.ForeignKey(SchoolModel, on_delete=models.CASCADE)
    password = models.CharField(max_length=13)
    imagepath = models.ImageField(max_length=100, default='default.jpg')
    slug = models.SlugField(max_length = 250,unique=True, null = True, blank = True) 
    section_id = models.IntegerField()
    contact_no = models.CharField(max_length=12)
    created_at = models.DateTimeField (auto_now=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        r = random.randint(0,100)
        self.slug = slugify(self.title) + '_' + str(r)
        super(StudentModel, self).save(*args, **kwargs)

class Assign_TeacherModel(models.Model):
    teacher = models.ForeignKey(TeacherModel,on_delete=models.CASCADE , related_name='assignteachers')
    classes = models.ForeignKey(TblClassModel , on_delete=models.CASCADE , related_name='assignclasses')
    course = models.ForeignKey(CourseModel , on_delete= models.CASCADE , related_name='assigncourses')

    def __str__(self):
        return '%d: %s' % (self.id, self.teacher.title)

class Enroll_StudentModel(models.Model):
    student = models.ForeignKey(StudentModel,on_delete=models.CASCADE )
    classes = models.ForeignKey(TblClassModel , on_delete=models.CASCADE , related_name='enrollStudents')
    course = models.ForeignKey(CourseModel , on_delete= models.CASCADE )

    def __str__(self):
        return '%s: %s' % (self.course.title, self.student.title)

class ClientFeedBackModel(models.Model):
    course = models.ForeignKey(CourseModel, on_delete=models.CASCADE, related_name= 'courses')
    description = models.CharField(max_length=500)
    client = models.ForeignKey(StudentModel,on_delete=models.CASCADE , related_name='students')
    option = [('Good','Good'),('Average','Average'),('Bad','Bad')]
    rating = models.CharField(max_length=100, choices=option, default='none')
    created_at = models.DateTimeField (auto_now=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '%s: %s' % (self.course.title, self.client.title)

class ContactFormModel(models.Model):
    student = models.ForeignKey(StudentModel,on_delete=models.CASCADE )
    query = models.TextField(default='None')
    response = models.TextField(default='None')
    created_at = models.DateTimeField (auto_now=True)
    updated_at = models.DateTimeField(auto_now_add=True)
