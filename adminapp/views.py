from django.shortcuts import render

# Create your views here.

from django.shortcuts import render
from adminapp.models import *
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser
from django.http import Http404
from .serializers import *
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
##
# Create your views here.

# def dashboard(request):
#     return render(request,'dashboard.html')

class Add_Teacher(APIView):

    def post(self, request, format = None):
        serializer = TeacherSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class Add_Student(APIView):
    def post(self, request, format=None):
        serializer = StudentSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class Assign_Course(APIView):
    def post(self, request, format=None):
        serializer = AssignCourseSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class Add_Course(APIView):
    def post(self, request, format=None):
        serializer = CourseSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class View_Course(APIView):
    def get(self, request, format=None):
        courses = CourseModel.objects.all()
        serializer = ViewCourseSerializer(courses, many=True)
        return Response(serializer.data)

class Edit_Course(APIView):
    def get_object(self, slug):
        try:
            return CourseModel.objects.get(slug=slug)
        except CourseModel.DoesNotExist:
            raise Http404

    def get(self, request, slug, format=None):
        course = self.get_object(slug)
        serializer = ViewCourseSerializer(course)
        return Response(serializer.data)

    def put(self,request,slug):
        data = self.get_object(slug)
        serializer = CourseSerializer(data = request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class Delete_Course(APIView):
    def get_object(self, slug):
        try:
            return CourseModel.objects.get(slug=slug)
        except CourseModel.DoesNotExist:
            raise Http404
    def delete(self, request, slug, format=None):
        course = self.get_object(slug)
        course.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class Add_Class(APIView):
    def post(self, request, format=None):
        serializer = TblClassSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class View_Class(APIView):
    def get(self, request, format=None):
        courses = TblClassModel.objects.all()
        serializer = TblClassSerializer(courses, many=True)
        return Response(serializer.data)

class Edit_Class(APIView):
    def get_object(self, slug):
        try:
            return TblClassModel.objects.get(slug=slug)
        except TblClassModel.DoesNotExist:
            raise Http404

    def get(self, request, slug, format=None):
        classes = self.get_object(slug)
        serializer = TblClassSerializer(classes)
        return Response(serializer.data)

    def put(self,request,slug):
        data = self.get_object(slug)
        serializer = TblClassSerializer(data = request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class Delete_Class(APIView):
    def get_object(self, slug):
        try:
            return TblClassModel.objects.get(slug=slug)
        except TblClassModel.DoesNotExist:
            raise Http404
    def delete(self, request, slug, format=None):
        classes = self.get_object(slug)
        classes.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class Unassign_Course(APIView):
    def delete(self, request,format=None):
        Assign_TeacherModel.objects.all().delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class Enroll_Student(APIView):
    def post(self, request, format=None):
        serializer = EnrollStudentSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class Modify_Teacher(APIView):
    def get_object(self, slug):
        try:
            return TeacherModel.objects.get(slug=slug)
        except TeacherModel.DoesNotExist:
            raise Http404

    def get(self, request, slug, format=None):
        teachers = self.get_object(slug)
        serializer = TeacherSerializer(teachers)
        return Response(serializer.data)

    def put(self,request,slug):
        data = self.get_object(slug)
        serializer = TeacherSerializer(data = request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class Modify_Student(APIView):
    def get_object(self, slug):
        try:
            return StudentModel.objects.get(slug=slug)
        except StudentModel.DoesNotExist:
            raise Http404

    def get(self, request, slug, format=None):
        students = self.get_object(slug)
        serializer = StudentSerializer(students)
        return Response(serializer.data)

    def put(self,request,slug):
        data = self.get_object(slug)
        serializer = StudentSerializer(data = request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class Delete_Teacher(APIView):
    def get_object(self, slug):
        try:
            return TeacherModel.objects.get(slug=slug)
        except TeacherModel.DoesNotExist:
            raise Http404
    def delete(self, request, slug, format=None):
        teachers = self.get_object(slug)
        teachers.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class Delete_Student(APIView):
    def get_object(self, slug):
        try:
            return StudentModel.objects.get(slug=slug)
        except StudentModel.DoesNotExist:
            raise Http404
    def delete(self, request, slug, format=None):
        students = self.get_object(slug)
        students.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class feedback(APIView):
    def get(self, request, format=None):
        feeds = ClientFeedBackModel.objects.all()
        serializer = FeedbackSerializer(feeds, many=True)
        return Response(serializer.data)

class View_Queries(APIView):
    def get(self, request, format=None):
        datas = ContactFormModel.objects.all()
        serializer = ViewQuerySerializer(datas, many=True)
        return Response(serializer.data)

class Respond_Queries(APIView):
    def get_object(self, pk):
        try:
            return ContactFormModel.objects.get(pk=pk)
        except ContactFormModel.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        datas = self.get_object(pk)
        serializer = ViewQuerySerializer(datas)
        return Response(serializer.data)

    def put(self,request,pk):
        data = self.get_object(pk)
        serializer = ResponseSerializer(data = request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
