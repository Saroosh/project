from rest_framework import serializers
from adminapp.models import *

class SchoolSerializer(serializers.ModelSerializer):
    class Meta:
        model = SchoolModel
        fields = '__all__'

class TblClassSerializer(serializers.ModelSerializer):
    enrollStudents = serializers.StringRelatedField(many=True)
    class Meta:
        model = TblClassModel
        fields = ['id' , 'title' , 'school','enrollStudents']

class TeacherSerializer(serializers.ModelSerializer):
    class Meta:
        model = TeacherModel
        fields = '__all__'

class StudentSerializer(serializers.ModelSerializer):
    class Meta:
        model = StudentModel
        fields = '__all__'

class CourseSerializer(serializers.ModelSerializer):
    class Meta:
        model = CourseModel
        fields = '__all__'

class ViewCourseSerializer(serializers.ModelSerializer):
    assigncourses = serializers.StringRelatedField(many=True)
    class Meta:
        model = CourseModel
        fields = ['id' , 'title','description' , 'code' , 'long_description', 'course_type' , 'classes' ,'assigncourses','created_at']

class AssignCourseSerializer(serializers.ModelSerializer):

    class Meta:
        model = Assign_TeacherModel
        fields = '__all__'

class EnrollStudentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Enroll_StudentModel
        fields = '__all__'

class FeedbackSerializer(serializers.ModelSerializer):
    class Meta:
        model = ClientFeedBackModel
        fields = ['id','course','description','client','rating','created_at']

class ViewQuerySerializer(serializers.ModelSerializer):
    class Meta:
        model = ContactFormModel
        fields = ['id','student','query','created_at']

class ResponseSerializer(serializers.ModelSerializer):
    class Meta:
        model = ContactFormModel
        fields = ['response','updated_at']
