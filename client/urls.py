from django.contrib import admin
from django.urls import path
from client.views import *
from client import views
from django.conf.urls import url, include

urlpatterns = [
    path('course', views.CoursesView.as_view(), name = "course"),
    path('course_detail/<int:pk>/', views.DetailCoursesView.as_view(), name = "course"),

    path('complaint', views.ComplaintView.as_view(), name = "complaint"),
    path('complaint_detail/<int:pk>/', views.DetailComplaintView.as_view(), name = "complaint"),

    path('article', views.ArticlesView.as_view(), name = "article"),
    path('article_detail/<int:pk>/', views.DetailArticlesView.as_view(), name = "article"),

    path('feedback', views.FeedbacksView.as_view(), name = "feedback"),
    path('feedback_detail/<int:pk>/', views.DetailFeedbacksView.as_view(), name = "feedback"),

    path('profile', views.ProfileView.as_view(), name = "profile"),
    #path('article_detail/<int:pk>/', views.DetailArticlesView.as_view(), name = "article"),
    path('announcement', views.AnnouncementView.as_view(), name = "announcement"),
    path('announcement_detail/<int:pk>/', views.DetailAnnouncementView.as_view(), name = "announcement"),

    path('onlinetest/', views.OnlineTestView.as_view(), name = "onlinetest"),
    path('onlinetest_detail/<int:pk>/', views.DetailOnlineTestView.as_view(), name = "onlinetest"),

]
