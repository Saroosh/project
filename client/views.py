from django.shortcuts import render

# ***************** API ****************
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser,FileUploadParser,MultiPartParser,FormParser
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status,viewsets,permissions
from rest_framework import generics
from superadmin.models import ArticleModel, ComplaintModel, CourseModel,ProfileModel, FeedbackModel, AnnouncementModel, OnlineTestModel
from superadmin.serializers import  ArticleSerializer, ComplaintSerializer,CourseSerializer, ProfileSerializer,FeedbackSerializer, AnnouncementSerializer, OnlineTestSerializer
from rest_framework.authentication import SessionAuthentication

# Create your views here.
class ProfileView(APIView):
    #permission_classes = (IsAuthenticated,SuperAdminPermission)
    #authentication_classes = (SessionAuthentication,)
    parser_classes = (MultiPartParser,FormParser)
    def get(self,request,format = None):
        data = ProfileModel.objects.all()
        serializer = ProfileSerializer (data, many = True)
        return Response(serializer.data,status = status.HTTP_200_OK)

class FeedbacksView(APIView):
    parser_classes = (MultiPartParser,FormParser)
    #permission_class = (permissions.IsAuthenticatedOrReadOnly)
    def get(self,request,format = None):
        data = FeedbackModel.objects.all()
        serializer = FeedbackSerializer (data, many = True)
        return Response(serializer.data,status = status.HTTP_200_OK)

    def post(self,request, format=None):
        serializer = FeedbackSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status = status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors,status = status.HTTP_400_BAD_REQUEST)


    def perform_create(self, serializer):
        serializer.save(User=self.request.user)


class DetailFeedbacksView(APIView):
    parser_classes = (MultiPartParser,FormParser)

    def get_object(self, slug):
        try:
            return FeedbackModel.objects.get(slug=slug)
        except FeedbackModel.DoesNotExist:
            raise Http404


    def get(self, request, slug, format=None):
        data = self.get_object(slug)
        serilizer = FeedbackSerializer(data)
        return Response(serilizer.data)

    def put(self,request,slug):
        data = self.get_object(slug)
        serializer = FeedbackSerializer(data , data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, slug, format=None):
        data = self.get_object(slug)
        data.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
    #permission_classes = [permissions.IsAuthenticatedOrReadOnly]


class ArticlesView(APIView):
    #permission_classes = (IsAuthenticated,SuperAdminPermission)
    #authentication_classes = (SessionAuthentication,)
    parser_classes = (MultiPartParser,FormParser)
    def get(self,request,format = None):
        data = ArticleModel.objects.all()
        serializer = ArticleSerializer (data, many = True)
        return Response(serializer.data,status = status.HTTP_200_OK)

    def post(self,request, format=None):
        print(request.data)
        serializer = ArticleSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status = status.HTTP_201_CREATED)
        else:
            return Response(serializer.data,status = status.HTTP_400_BAD_REQUEST)

class DetailArticlesView(APIView):
    parser_classes = (MultiPartParser,FormParser)

    def get_object(self, slug):
        try:
            return ArticleModel.objects.get(slug=slug)
        except ArticleModel.DoesNotExist:
            raise Http404


    def get(self, request, slug, format=None):
        data = self.get_object(slug)
        serilizer = ArticleSerializer(data)
        return Response(serilizer.data)

    def put(self,request,slug):
        data = self.get_object(slug)
        serializer = ArticleSerializer(data , data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, slug, format=None):
        data = self.get_object(slug)
        data.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
    #permission_classes = [permissions.IsAuthenticatedOrReadOnly]
class CoursesView(APIView):
    parser_classes = (MultiPartParser,FormParser)
    def get(self,request,format = None):
        data = CourseModel.objects.all()
        serializer = CourseSerializer(data, many = True)
        return Response(serializer.data,status = status.HTTP_200_OK)

    def post(self,request, format=None):
        serializer = CourseSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status = status.HTTP_201_CREATED)
        else:
            return Response(serializer.data,status = status.HTTP_400_BAD_REQUEST)

    def perform_create(self, serializer):
        serializer.save(User=self.request.user)

class DetailCoursesView(APIView):
    parser_classes = (MultiPartParser,FormParser)

    def get_object(self, slug):
        try:
            return CourseModel.objects.get(slug=slug)
        except CourseModel.DoesNotExist:
            raise Http404


    def get(self, request, slug, format=None):
        data = self.get_object(slug)
        serilizer = CourseSerializer(data)
        return Response(serilizer.data)

    def put(self,request,slug):
        data = self.get_object(slug)
        serializer = CourseSerializer(data , data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, slug, format=None):
        data = self.get_object(slug)
        data.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]

class ComplaintView(APIView):
    parser_classes = (MultiPartParser,FormParser)
    def get(self,request,format = None):
        data = ComplaintModel.objects.all()
        serializer = ComplaintSerializer (data, many = True)
        return Response(serializer.data,status = status.HTTP_200_OK)

    def post(self,request, format=None):
        serializer = ComplaintSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status = status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors,status = status.HTTP_400_BAD_REQUEST)

class DetailComplaintView(APIView):
    #permission_classes = (IsAuthenticated,SuperAdminPermission)
    #authentication_classes = (SessionAuthentication,)
    parser_classes = (MultiPartParser,FormParser)
    def get_object(self, pk):
        try:
            return ComplaintModel.objects.get(id=pk)
        except ComplaintModel.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        data = self.get_object(pk)
        serilizer = ComplaintSerializer(data)
        return Response(serilizer.data)

    def put(self,request,pk):
        data = self.get_object(pk)
        serializer = ComplaintSerializer(data , data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        data = self.get_object(pk)
        data.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class AnnouncementView(APIView):
    parser_classes = (MultiPartParser,FormParser)

    def get(self,request,format = None):
        data = AnnouncementModel.objects.all()
        serializer = AnnouncementSerializer (data, many = True)
        return Response(serializer.data,status = status.HTTP_200_OK)

    def post(self,request, format=None):
        serializer = AnnouncementSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status = status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors,status = status.HTTP_400_BAD_REQUEST)

class DetailAnnouncementView(APIView):
    parser_classes = (MultiPartParser,FormParser)


    def get_object(self, pk):
        try:
            return AnnouncementModel.objects.get(id=pk)
        except AnnouncementModel.DoesNotExist:
            raise Http404


    def get(self, request, pk, format=None):
        data = self.get_object(pk)
        serilizer = AnnouncementSerializer(data)
        return Response(serilizer.data)

    def put(self,request,pk):
        data = self.get_object(pk)
        serializer = AnnouncementSerializer(data , data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        data = self.get_object(pk)
        data.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class OnlineTestView(APIView):
    #permission_classes = (IsAuthenticated,SuperAdminPermission)
    #authentication_classes = (SessionAuthentication,)
    parser_classes = (MultiPartParser,FormParser)

    def get(self,request,format = None):
        data = OnlineTestModel.objects.all()
        serializer = OnlineTestSerializer(data, many = True)
        return Response(serializer.data,status = status.HTTP_200_OK)

    def post(self,request, format=None):
        serializer = OnlineTestSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status = status.HTTP_201_CREATED)
        else:
            return Response(serializer.data,status = status.HTTP_400_BAD_REQUEST)
class DetailOnlineTestView(APIView):
    parser_classes = (MultiPartParser,FormParser)


    def get_object(self, pk):
        try:
            return OnlineTestModel.objects.get(id=pk)
        except OnlineTestModel.DoesNotExist:
            raise Http404


    def get(self, request, pk, format=None):
        data = self.get_object(pk)
        serilizer = OnlineTestSerializer(data)
        return Response(serilizer.data)

    def put(self,request,pk):
        data = self.get_object(pk)
        serializer = OnlineTestSerializer(data , data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        data = self.get_object(pk)
        data.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
