from django.db import models
from django.contrib.auth.models import User
from django.contrib.auth.models import AbstractUser
from django.template.defaultfilters import slugify
from adminapp.models import *
from django.contrib.auth.models import AbstractUser
import random
# Create your models here.

class Role(models.Model):
    title = models.CharField(max_length=50)
    slug = models.SlugField(max_length=250,null=True, unique=True, blank=True)
    created_at = models.DateTimeField (auto_now=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.title
    def get_absolute_url(self):
        kwargs = {
            'slug': self.slug
        }
        return reverse('role_detail', kwargs=kwargs)
    def save(self, *args, **kwargs):
        value = self.title
        self.slug = slugify(value,)
        super().save(*args, **kwargs)

class UserModel(models.Model):
    username = models.CharField(unique=True,null=False,max_length=50)
    slug = models.SlugField(max_length=250,null=True, unique=True)
    email = models.EmailField(unique=True,null=False,blank=False)
    password = models.CharField(max_length=50,null=False,unique=True,blank=False)
    #role = models.ForeignKey(Role,null=False,blank=False, on_delete=models.CASCADE)


class ProfileModel(models.Model):

    #users = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField(max_length=50)
    slug = models.SlugField(max_length=250,null=True, unique=True, blank=True)
    image = models.ImageField()
    address = models.CharField(max_length=500)
    address2 = models.CharField(max_length=500,null=True)
    #role = models.ForeignKey(Role,null=True,blank=True, default=0, on_delete=models.CASCADE)
    contact_no = models.CharField(max_length=12)
    created_at = models.DateTimeField (auto_now=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        kwargs = {
            'slug': self.slug
        }
        return reverse('article_detail', kwargs=kwargs)
    def save(self, *args, **kwargs):
        value = self.title
        self.slug = slugify(value,)
        super().save(*args, **kwargs)


class ComplaintModel(models.Model):

    #type = models.CharField(max_length=50)
    title = models.CharField(max_length=50)
    #image = models.ImageField(upload_to ="complaint/", blank=False, null=False,default="/image.jpg/")
    slug = models.SlugField(max_length=250,null=True, unique=True)
    short_description = models.CharField(max_length=700)
    long_description = models.CharField(max_length=1400)
    created_at = models.DateTimeField (auto_now=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        kwargs = {
            'slug': self.slug
        }
        return reverse('article_detail', kwargs=kwargs)
    def save(self, *args, **kwargs):
        value = self.title
        self.slug = slugify(value,)
        super().save(*args, **kwargs)

class ScheduleModel(models.Model):

    type = models.CharField(max_length=50)
    title = models.CharField(max_length=50)
    slug = models.SlugField(max_length=250,null=True, unique=True)
    image = models.ImageField(upload_to ="article/", blank=False, null=False,default="/image.jpg/")
    short_description = models.CharField(max_length=700)
    long_description = models.CharField(max_length=1400)
    created_at = models.DateTimeField (auto_now=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        kwargs = {
            'slug': self.slug
        }
        return reverse('article_detail', kwargs=kwargs)
    def save(self, *args, **kwargs):
        value = self.title
        self.slug = slugify(value,)
        super().save(*args, **kwargs)





"""
    def get_absolute_url(self):
        kwargs = {
            'slug': self.slug
        }
        return reverse('course_detail', kwargs=kwargs)
    def save(self, *args, **kwargs):
        value = self.title
        self.slug = slugify(value,)
        super().save(*args, **kwargs)
"""
class ArticleModel(models.Model):

    title = models.CharField(max_length=50)
    slug = models.SlugField(max_length=250,null=True, unique=True)
    image = models.ImageField(upload_to ="article/", blank=False, null=True)
    user = models.ForeignKey(User,on_delete=models.CASCADE, null=True)
    short_description = models.CharField(max_length=700)
    long_description = models.CharField(max_length=1400)
    created_at = models.DateTimeField (auto_now=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        kwargs = {
            'slug': self.slug
        }
        return reverse('article_detail', kwargs=kwargs)
    def save(self, *args, **kwargs):
        value = self.title
        self.slug = slugify(value,)
        super().save(*args, **kwargs)

class BlogModel(models.Model):

    title = models.CharField(max_length=50)
    slug = models.SlugField(max_length=250,null=True, unique=True)
    image = models.ImageField(upload_to ="blog/", blank=False, null=True)
    #user = models.ForeignKey(User,on_delete=models.CASCADE, null=True)
    short_description = models.CharField(max_length=700)
    long_description = models.CharField(max_length=1400)
    created_at = models.DateTimeField (auto_now=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title
    def get_absolute_url(self):
        kwargs = {
            'slug': self.slug
        }
        return reverse('blog_detail', kwargs=kwargs)
    def save(self, *args, **kwargs):
        value = self.title
        self.slug = slugify(value,)
        super().save(*args, **kwargs)


class EventModel(models.Model):

    title = models.CharField(max_length=50)
    slug = models.SlugField(max_length=250,null=True, unique=True)
    image = models.ImageField(upload_to ="blog/", blank=False, null=True)
    #user = models.ForeignKey(User,on_delete=models.CASCADE, null=True)
    short_description = models.CharField(max_length=700)
    long_description = models.CharField(max_length=1400)
    created_at = models.DateTimeField (auto_now=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title
    def get_absolute_url(self):
        kwargs = {
            'slug': self.slug
        }
        return reverse('event_detail', kwargs=kwargs)
    def save(self, *args, **kwargs):
        value = self.title
        self.slug = slugify(value,)
        super().save(*args, **kwargs)


class ClassStandardModel(models.Model):

    title = models.CharField(max_length=50,default="anything")
    slug = models.SlugField(max_length=250,null=True, unique=True)
    #teacher = models.ForeignKey(User,on_delete=models.CASCADE,default=1)
    short_description = models.CharField(max_length=700)
    long_description = models.CharField(max_length=1400)
    created_at = models.DateTimeField(auto_now=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title
    def get_absolute_url(self):
        kwargs = {
            'slug': self.slug
        }
        return reverse('class-standard_detail', kwargs=kwargs)
    def save(self, *args, **kwargs):
        value = self.title
        self.slug = slugify(value,)
        super().save(*args, **kwargs)

class SchoolModel(models.Model):
    title = models.CharField(max_length=50)
    name = models.CharField(max_length=50)
    slug = models.SlugField(max_length=250,null=True, unique=True)
    contact = models.CharField(max_length=50)
    email = models.CharField(max_length=50)
    password = models.CharField(max_length=700)
    created_at = models.DateTimeField(auto_now=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        kwargs = {
            'slug': self.slug
        }
        return reverse('article_detail', kwargs=kwargs)
    def save(self, *args, **kwargs):
        value = self.title
        self.slug = slugify(value,)
        super().save(*args, **kwargs)

# class SubjectModel(models.Model):
#     title = models.CharField(max_length=50,default="anything")
#     slug = models.SlugField(max_length=250,null=True, unique=True)
#     #teacher = models.ForeignKey(User,on_delete=models.CASCADE,default=1)
#     #class_standard = models.ForeignKey(ClassStandardModel,on_delete=models.CASCADE,default=1)
#     short_discription = models.CharField(max_length=700)
#     long_discription = models.CharField(max_length=1400)
#     created_at = models.DateTimeField (auto_now=True)
#     updated_at = models.DateTimeField(auto_now_add=True)
#
#     def __str__(self):
#         return self.title
#     def get_absolute_url(self):
#         kwargs = {
#             'slug': self.slug
#         }
#
#         return reverse('subject_detail', kwargs=kwargs)
#     def save(self, *args, **kwargs):
#         value = self.title
#         self.slug = slugify(value,)
#         super().save(*args, **kwargs)
class CallobrationsModel(models.Model):

    title = models.CharField(max_length=50)
    slug = models.SlugField(max_length=250,null=True, unique=True)
    #image = models.ImageField(upload_to ="blog/", blank=False, null=True)
    #user = models.ForeignKey(User,on_delete=models.CASCADE, null=True)
    short_description = models.CharField(max_length=700)
    long_description = models.CharField(max_length=1400)
    created_at = models.DateTimeField (auto_now=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title
    def get_absolute_url(self):
        kwargs = {
            'slug': self.slug
        }
        return reverse('event_detail', kwargs=kwargs)
    def save(self, *args, **kwargs):
        value = self.title
        self.slug = slugify(value,)
        super().save(*args, **kwargs)

# RelationShip: Course->Subject->OnlineTest->Question->Options
class CourseModel(models.Model):
    # test = models.ForeignKey(OnlineTestModel,on_delete=models.CASCADE,max_length=700,null=True)
    title = models.CharField(max_length=700) 
    code = models.CharField(max_length=50)
    slug = models.SlugField(max_length=250,null=True, unique=True)
    created_at = models.DateTimeField (auto_now=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        kwargs = {
            'slug': self.slug
        }
        return reverse('article_detail', kwargs=kwargs)
    def save(self, *args, **kwargs):
        value = self.title
        self.slug = slugify(value,)
        super().save(*args, **kwargs)


class ContentModel(models.Model):
    title = models.CharField(max_length=700)
    course_id = models.ForeignKey(CourseModel,related_name="Contents",on_delete=models.CASCADE,max_length=700)
    video= models.FileField(max_length=100, default='no video', upload_to='media')
    long_description = models.CharField(max_length=1400)
    slug = models.SlugField(max_length=250,null=True, unique=True)
    created_at = models.DateTimeField (auto_now=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.slug
    def get_absolute_url(self):
        kwargs = {
            'slug': self.slug
        }
        return reverse('article_detail', kwargs=kwargs)
    def save(self, *args, **kwargs):
        value = self.title
        self.slug = slugify(value,)
        super().save(*args, **kwargs)


class OnlineTestModel(models.Model):
    title = models.CharField(max_length=700)
    content_id = models.ForeignKey(ContentModel,related_name="questions",on_delete=models.CASCADE,max_length=700)
    # questions = models.CharField(max_length=700)
    slug = models.SlugField(max_length=250,null=True, unique=True)
    #options = models.ForeignKey(QuestionModel,on_delete=models.CASCADE, null=False)
    created_at = models.DateTimeField (auto_now=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.questions
    def get_absolute_url(self):
        kwargs = {
            'slug': self.slug
        }
        return reverse('article_detail', kwargs=kwargs)
    def save(self, *args, **kwargs):
        value = self.title
        self.slug = slugify(value,)
        super().save(*args, **kwargs)


class QuestionModel(models.Model):
    title = models.CharField(max_length=700)
    test_id = models.ForeignKey(OnlineTestModel,on_delete=models.CASCADE,max_length=700)
    option1 = models.CharField(max_length=700)
    option2 = models.CharField(max_length=700)
    option2 = models.CharField(max_length=700)
    option4 = models.CharField(max_length=700)
    slug = models.SlugField(max_length=250,null=True, unique=True)
    created_at = models.DateTimeField (auto_now=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.questions
    def get_absolute_url(self):
        kwargs = {
            'slug': self.slug
        }
        return reverse('article_detail', kwargs=kwargs)
    def save(self, *args, **kwargs):
        value = self.title
        self.slug = slugify(value,)
        super().save(*args, **kwargs)


class OptionModel(models.Model):
    # questions = models.ForeignKey(QuestionModel,on_delete=models.CASCADE,max_length=700)

    #answer = models.CharField(max_length=700, )
    slug = models.SlugField(max_length=250,null=True, unique=True)
    created_at = models.DateTimeField (auto_now=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.questions
    # def get_absolute_url(self):
    #     kwargs = {
    #         'slug': self.slug
    #     }
    #     return reverse('article_detail', kwargs=kwargs)
    # def save(self, *args, **kwargs):
    #     value = self.questions
    #     self.slug = slugify(value,)
    #     super().save(*args, **kwargs)


# class QueriesModel(models.Model):
#     title = models.CharField(max_length=50,default="anything")
#     slug = models.SlugField(max_length=250,null=True, unique=True)
#     #teacher = models.ForeignKey(User,on_delete=models.CASCADE,default=1)
#     short_description = models.CharField(max_length=700)
#     long_description = models.CharField(max_length=1400)
#     created_at = models.DateTimeField (auto_now=True, null=True)
#     updated_at = models.DateTimeField(auto_now_add=True, null=True)
#
#     def __str__(self):
#         return self.title
#     def get_absolute_url(self):
#         kwargs = {
#             'slug': self.slug
#         }
#         return reverse('event_detail', kwargs=kwargs)
#     def save(self, *args, **kwargs):
#         value = self.title
#         self.slug = slugify(value,)
#         super().save(*args, **kwargs)

class AnnouncementModel(models.Model):

    title = models.CharField(max_length=50)
    slug = models.SlugField(max_length=250,null=True, unique=True)
    short_description = models.CharField(max_length=700)
    long_description = models.CharField(max_length=1400)
    created_at = models.DateTimeField (auto_now=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title
    def get_absolute_url(self):
        kwargs = {
            'slug': self.slug
        }
        return reverse('event_detail', kwargs=kwargs)
    def save(self, *args, **kwargs):
        value = self.title
        self.slug = slugify(value,)
        super().save(*args, **kwargs)
class_attendance = (
    ('Present','Present'),
    ('Absent','Absent'),
)


class AttendanceModel(models.Model):

    title = models.CharField(max_length=50)
    slug = models.SlugField(max_length=250,null=True, unique=True)
    short_description = models.CharField(max_length=700)
    long_description = models.CharField(max_length=1400)
    mark_attendance = models.CharField(max_length=50, choices=class_attendance)
    created_at = models.DateTimeField (auto_now=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title
    def get_absolute_url(self):
        kwargs = {
            'slug': self.slug
        }
        return reverse('event_detail', kwargs=kwargs)
    def save(self, *args, **kwargs):
        value = self.title
        self.slug = slugify(value,)
        super().save(*args, **kwargs)


class KidStoryModel(models.Model):

    title = models.CharField(max_length=50)
    slug = models.SlugField(max_length=250,null=True, unique=True)
    image = models.ImageField(upload_to ="blog/", blank=False, null=True)
    #user = models.ForeignKey(User,on_delete=models.CASCADE, null=True)
    short_description = models.CharField(max_length=700)
    long_description = models.CharField(max_length=1400)
    created_at = models.DateTimeField (auto_now=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title
    def get_absolute_url(self):
        kwargs = {
            'slug': self.slug
        }
        return reverse('event_detail', kwargs=kwargs)
    def save(self, *args, **kwargs):
        value = self.title
        self.slug = slugify(value,)
        super().save(*args, **kwargs)


class KidTalentModel(models.Model):

    title = models.CharField(max_length=50)
    slug = models.SlugField(max_length=250,null=True, unique=True)
    image = models.ImageField(upload_to ="blog/", blank=False, null=True)
    #user = models.ForeignKey(User,on_delete=models.CASCADE, null=True)
    short_description = models.CharField(max_length=700)
    long_description = models.CharField(max_length=1400)
    created_at = models.DateTimeField (auto_now=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title
    def get_absolute_url(self):
        kwargs = {
            'slug': self.slug
        }
        return reverse('event_detail', kwargs=kwargs)
    def save(self, *args, **kwargs):
        value = self.title
        self.slug = slugify(value,)
        super().save(*args, **kwargs)

class QuizContextModel(models.Model):

    title = models.CharField(max_length=50)
    slug = models.SlugField(max_length=250,null=True, unique=True)
    #course = models.ForeignKey(CourseModel,null=False,blank=False, on_delete=models.CASCADE)
    short_description = models.CharField(max_length=700)
    long_description = models.CharField(max_length=1400)
    created_at = models.DateTimeField (auto_now=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.title
    def get_absolute_url(self):
        kwargs = {
            'slug': self.slug
        }
        return reverse('blog_detail', kwargs=kwargs)
    def save(self, *args, **kwargs):
        value = self.title
        self.slug = slugify(value,)
        super().save(*args, **kwargs)


class ApprovalModel(models.Model):

    title = models.CharField(max_length=50)
    slug = models.SlugField(max_length=250,null=True, unique=True)
    type = models.CharField(max_length=50)
    short_description = models.CharField(max_length=700)
    long_description = models.CharField(max_length=1400)
    created_at = models.DateTimeField (auto_now=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.title
    def get_absolute_url(self):
        kwargs = {
            'slug': self.slug
        }
        return reverse('blog_detail', kwargs=kwargs)
    def save(self, *args, **kwargs):
        value = self.title
        self.slug = slugify(value,)
        super().save(*args, **kwargs)
class BusinessOfferModel(models.Model):

    title = models.CharField(max_length=50)
    slug = models.SlugField(max_length=250,null=True, unique=True)
    image = models.ImageField(upload_to ="BusinessOffer/", blank=False, null=True)
    #user = models.ForeignKey(User,on_delete=models.CASCADE, null=True)
    short_description = models.CharField(max_length=700)
    long_description = models.CharField(max_length=1400)
    created_at = models.DateTimeField (auto_now=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title
    def get_absolute_url(self):
        kwargs = {
            'slug': self.slug
        }
        return reverse('blog_detail', kwargs=kwargs)
    def save(self, *args, **kwargs):
        value = self.title
        self.slug = slugify(value,)
        super().save(*args, **kwargs)

class DepartmentModel(models.Model):

    title = models.CharField(max_length=50)
    slug = models.SlugField(max_length=250,null=True, unique=True)
    #title = models.ForeignKey(TeamModel,on_delete=models.CASCADE,default=1)
    created_at = models.DateTimeField (auto_now=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.title
    def get_absolute_url(self):
        kwargs = {
            'slug': self.slug
        }
        return reverse('department_detail', kwargs=kwargs)
    def save(self, *args, **kwargs):
        value = self.title
        self.slug = slugify(value,)
        super().save(*args, **kwargs)

class TeamModel(models.Model):

    title = models.CharField(max_length=50)
    slug = models.SlugField(max_length=250,null=True, unique=True)
    image = models.ImageField(upload_to ="blog/", blank=False, null=True)
    user = models.ForeignKey(User,on_delete=models.CASCADE, null=True)
    short_description = models.CharField(max_length=700)
    long_description = models.CharField(max_length=1400)
    created_at = models.DateTimeField (auto_now=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.title
    def get_absolute_url(self):
        kwargs = {
            'slug': self.slug
        }
        return reverse('event_detail', kwargs=kwargs)
    def save(self, *args, **kwargs):
        value = self.title
        self.slug = slugify(value,)
        super().save(*args, **kwargs)

class AdvisoryBoardModel(models.Model):

    title = models.CharField(max_length=50)
    slug = models.SlugField(max_length=250,null=True, unique=True)
    designation = models.CharField(max_length=50)
    short_description = models.CharField(max_length=700)
    long_description = models.CharField(max_length=1400)
    created_at = models.DateTimeField (auto_now=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.title
    def get_absolute_url(self):
        kwargs = {
            'slug': self.slug
        }
        return reverse('event_detail', kwargs=kwargs)
    def save(self, *args, **kwargs):
        value = self.title
        self.slug = slugify(value,)
        super().save(*args, **kwargs)


class FeedbackModel(models.Model):

    title = models.CharField(max_length=50)
    slug = models.SlugField(max_length=250,null=True, unique=True)
    name= models.CharField(max_length=50)
    rating= models.CharField(max_length=50)
    short_description = models.CharField(max_length=700)
    long_description = models.CharField(max_length=1400)
    created_at = models.DateTimeField (auto_now=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.title
    def get_absolute_url(self):
        kwargs = {
            'slug': self.slug
        }
        return reverse('event_detail', kwargs=kwargs)
    def save(self, *args, **kwargs):
        value = self.title
        self.slug = slugify(value,)
        super().save(*args, **kwargs)

class WebsiteAdModel(models.Model):

    title = models.CharField(max_length=50)
    slug = models.SlugField(max_length=250,null=True, unique=True)
    image = models.ImageField(upload_to ="blog/", blank=False, null=True)
    #user = models.ForeignKey(User,on_delete=models.CASCADE, null=True)
    short_description = models.CharField(max_length=700)
    long_description = models.CharField(max_length=1400)
    created_at = models.DateTimeField (auto_now=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title
    def get_absolute_url(self):
        kwargs = {
            'slug': self.slug
        }
        return reverse('event_detail', kwargs=kwargs)
    def save(self, *args, **kwargs):
        value = self.title
        self.slug = slugify(value,)
        super().save(*args, **kwargs)
