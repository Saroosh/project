from rest_framework import serializers
from .models import *
from django.contrib.auth.models import User




class QuestionSerializer(serializers.ModelSerializer):
    class Meta:
        model = QuestionModel
        fields = '__all__'

class OnlineTestSerializer(serializers.ModelSerializer):
    questions = QuestionSerializer(many=True, read_only=True)
    class Meta:
        model = OnlineTestModel
        fields = ('__all__')

class ContentSerializer(serializers.ModelSerializer):
    onlinetests = OnlineTestSerializer(many=True, read_only=True)
    class Meta:
        model = ContentModel
        fields = '__all__'


class CourseSerializer(serializers.ModelSerializer):
    Contents = ContentSerializer(many=True, read_only=True)
    
    class Meta:
        model = CourseModel
        fields = ('__all__')

class OptionSerializer(serializers.ModelSerializer):
    class Meta:
        model = OptionModel
        fields = '__all__'
class SchoolSerializer(serializers.ModelSerializer):
    class Meta:
        model = SchoolModel
        fields = '__all__'

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserModel
        fields = '__all__'

class ProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProfileModel
        fields = '__all__'

class ArticleSerializer(serializers.ModelSerializer):
    class Meta:
        model = ArticleModel
        fields = ('__all__')

class BlogSerializer(serializers.ModelSerializer):
    class Meta:
        model = BlogModel
        fields = ('__all__')

class EventSerializer(serializers.ModelSerializer):
    class Meta:
        model = EventModel
        fields = ('__all__')

class CallobrationsSerializer(serializers.ModelSerializer):
    class Meta:
        model = CallobrationsModel
        fields = ('__all__')

# class QueriesSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = QueriesModel
#         fields = ('__all__')

class AnnouncementSerializer(serializers.ModelSerializer):
    class Meta:
        model = AnnouncementModel
        fields = ('__all__')

class ComplaintSerializer(serializers.ModelSerializer):
    class Meta:
        model = ComplaintModel
        fields = ('__all__')

class ScheduleSerializer(serializers.ModelSerializer):
    class Meta:
        model = ScheduleModel
        fields = ('__all__')

class AttendanceSerializer(serializers.ModelSerializer):
    class Meta:
        model = AttendanceModel
        fields = ('__all__')

class FeedbackSerializer(serializers.ModelSerializer):
    class Meta:
        model = FeedbackModel
        fields = ('__all__')

class AdvisoryBoardSerializer(serializers.ModelSerializer):
    class Meta:
        model = AdvisoryBoardModel
        fields = ('__all__')

class TeamSerializer(serializers.ModelSerializer):
    class Meta:
        model = TeamModel
        fields = ('__all__')

class DepartmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = DepartmentModel
        fields = ('__all__')

class BusinessOfferSerializer(serializers.ModelSerializer):
    class Meta:
        model = BusinessOfferModel
        fields = ('__all__')

class ApprovalSerializer(serializers.ModelSerializer):
    class Meta:
        model = ApprovalModel
        fields = ('__all__')


class  KidTalentSerializer(serializers.ModelSerializer):
    class Meta:
        model = KidTalentModel
        fields = ('__all__')

class  KidStorySerializer(serializers.ModelSerializer):
    class Meta:
        model = KidStoryModel
        fields = ('__all__')

class  QuizContextSerializer(serializers.ModelSerializer):
    class Meta:
        model = QuizContextModel
        fields = ('__all__')


class WebsiteAdSerializer(serializers.ModelSerializer):
    class Meta:
        model = WebsiteAdModel
        fields = ('__all__')
