from django.shortcuts import render

# ***************** API ****************
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser,FileUploadParser,MultiPartParser,FormParser
from .models import *
from django.http import Http404
from .serializers import *
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status,viewsets,permissions
from rest_framework import generics

from .custompermissions import *
from rest_framework.authentication import SessionAuthentication

# Create your views here.

class UserListView(generics.ListAPIView):
    parser_classes = (MultiPartParser,FormParser)
    queryset = UserModel.objects.all()
    serializer_class = UserSerializer

class UserDetalView(generics.RetrieveAPIView):
    parser_classes = (MultiPartParser,FormParser)
    queryset = UserModel.objects.all()
    serializer_class = UserSerializer

class ProfileView(APIView):
    #permission_classes = (IsAuthenticated,SuperAdminPermission)
    #authentication_classes = (SessionAuthentication,)
    parser_classes = (MultiPartParser,FormParser)
    def get(self,request,format = None):
        data = ProfileModel.objects.all()
        serializer = ProfileSerializer (data, many = True)
        return Response(serializer.data,status = status.HTTP_200_OK)

class ArticlesView(APIView):
    #permission_classes = (IsAuthenticated,SuperAdminPermission)
    #authentication_classes = (SessionAuthentication,)
    parser_classes = (MultiPartParser,FormParser)
    def get(self,request,format = None):
        data = ArticleModel.objects.all()
        serializer = ArticleSerializer (data, many = True)
        return Response(serializer.data,status = status.HTTP_200_OK)

    def post(self,request, format=None):
        print(request.data)
        serializer = ArticleSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status = status.HTTP_201_CREATED)
        else:
            return Response(serializer.data,status = status.HTTP_400_BAD_REQUEST)

class DetailArticlesView(APIView):
    parser_classes = (MultiPartParser,FormParser)

    def get_object(self, slug):
        try:
            return ArticleModel.objects.get(slug=slug)
        except ArticleModel.DoesNotExist:
            raise Http404


    def get(self, request, slug, format=None):
        data = self.get_object(slug)
        serilizer = ArticleSerializer(data)
        return Response(serilizer.data)

    def put(self,request,slug):
        data = self.get_object(slug)
        serializer = ArticleSerializer(data , data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, slug, format=None):
        data = self.get_object(slug)
        data.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
    #permission_classes = [permissions.IsAuthenticatedOrReadOnly]

class ContentsView(APIView):
    #permission_classes = (IsAuthenticated,SuperAdminPermission)
    #authentication_classes = (SessionAuthentication,)
    parser_classes = (MultiPartParser,FormParser)
    def get(self,request,format = None):
        data = ContentModel.objects.all()
        serializer = ContentSerializer (data, many = True,context={'request': request})
        return Response(serializer.data,status = status.HTTP_200_OK)

    def post(self,request, format=None):
        print(request.data)
        serializer = ContentSerializer(data=request.data,context={'request': request})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status = status.HTTP_201_CREATED)
        else:
            return Response(serializer.data,status = status.HTTP_400_BAD_REQUEST)

class DetailContentsView(APIView):
    parser_classes = (MultiPartParser,FormParser)

    def get_object(self, slug):
        try:
            return ContentModel.objects.get(slug=slug)
        except ContentModel.DoesNotExist:
            raise Http404


    def get(self, request, slug, format=None):
        data = self.get_object(slug)
        serializer = ContentSerializer(data,context={'request': request})
        return Response(serializer.data)

    def put(self,request,slug):
        data = self.get_object(slug)
        serializer = ContentSerializer(data , data=request.data,context={'request': request})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, slug, format=None):
        data = self.get_object(slug)
        data.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
    #permission_classes = [permissions.IsAuthenticatedOrReadOnly]


class QuestionsView(APIView):
    #permission_classes = (IsAuthenticated,SuperAdminPermission)
    #authentication_classes = (SessionAuthentication,)
    parser_classes = (MultiPartParser,FormParser)
    def get(self,request,format = None):
        data = QuestionModel.objects.all()
        serializer = QuestionSerializer (data, many = True)
        return Response(serializer.data,status = status.HTTP_200_OK)

    def post(self,request, format=None):
        print(request.data)
        serializer = QuestionSerializer(data=request.data,context={'request': request})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status = status.HTTP_201_CREATED)
        else:
            return Response(serializer.data,status = status.HTTP_400_BAD_REQUEST)

class DetailQuestionsView(APIView):
    parser_classes = (MultiPartParser,FormParser)

    def get_object(self, slug):
        try:
            return QuestionModel.objects.get(slug=slug)
        except QuestionModel.DoesNotExist:
            raise Http404


    def get(self, request, slug, format=None):
        data = self.get_object(slug)
        serilizer = QuestionSerializer(data)
        return Response(serilizer.data)

    def put(self,request,slug):
        data = self.get_object(slug)
        serializer = QuestionSerializer(data , data=request.data,context={'request': request})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, slug, format=None):
        data = self.get_object(slug)
        data.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
    #permission_classes = [permissions.IsAuthenticatedOrReadOnly]


class OptionsView(APIView):
    #permission_classes = (IsAuthenticated,SuperAdminPermission)
    #authentication_classes = (SessionAuthentication,)
    parser_classes = (MultiPartParser,FormParser)
    def get(self,request,format = None):
        data = OptionModel.objects.all()
        serializer = OptionSerializer (data, many = True)
        return Response(serializer.data,status = status.HTTP_200_OK)

    def post(self,request, format=None):
        print(request.data)
        serializer = OptionSerializer(data=request.data,context={'request': request})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status = status.HTTP_201_CREATED)
        else:
            return Response(serializer.data,status = status.HTTP_400_BAD_REQUEST)

class DetailOptionsView(APIView):
    parser_classes = (MultiPartParser,FormParser)

    def get_object(self, slug):
        try:
            return OptionModel.objects.get(slug=slug)
        except OptionModel.DoesNotExist:
            raise Http404


    def get(self, request, slug, format=None):
        data = self.get_object(slug)
        serilizer = OptionSerializer(data)
        return Response(serilizer.data)

    def put(self,request,slug):
        data = self.get_object(slug)
        serializer = OptionSerializer(data , data=request.data,context={'request': request})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, slug, format=None):
        data = self.get_object(slug)
        data.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
    #permission_classes = [permissions.IsAuthenticatedOrReadOnly]


class SchoolsView(APIView):
    parser_classes = (MultiPartParser,FormParser)
    #permission_class = (permissions.IsAuthenticatedOrReadOnly)
    def get(self,request,format = None):
        data = SchoolModel.objects.all()
        serializer = SchoolSerializer (data, many = True)
        return Response(serializer.data,status = status.HTTP_200_OK)

    def post(self,request, format=None):
        serializer = SchoolSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status = status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors,status = status.HTTP_400_BAD_REQUEST)


    def perform_create(self, serializer):
        serializer.save(User=self.request.user)


class DetailSchoolsView(APIView):
    parser_classes = (MultiPartParser,FormParser)

    def get_object(self, slug):
        try:
            return SchoolModel.objects.get(slug=slug)
        except SchoolModel.DoesNotExist:
            raise Http404


    def get(self, request, slug, format=None):
        data = self.get_object(slug)
        serilizer = SchoolSerializer(data)
        return Response(serilizer.data)

    def put(self,request,slug):
        data = self.get_object(slug)
        serializer = SchoolSerializer(data , data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, slug, format=None):
        data = self.get_object(slug)
        data.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]



class BlogsView(APIView):
    #permission_classes = (IsAuthenticated,SuperAdminPermission)
    #authentication_classes = (SessionAuthentication,)
    parser_classes = (MultiPartParser,FormParser)
    def get(self,request,format = None):
        data = BlogModel.objects.all()
        serializer = BlogSerializer(data, many = True)
        return Response(serializer.data,status = status.HTTP_200_OK)

    def post(self,request, format=None):
        serializer = BlogSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status = status.HTTP_201_CREATED)
        else:
            return Response(serializer.data,status = status.HTTP_400_BAD_REQUEST)

    def perform_create(self, serializer):
        serializer.save(User=self.request.user)

class DetailBlogsView(APIView):
    #permission_classes = (IsAuthenticated,SuperAdminPermission)
    #authentication_classes = (SessionAuthentication,)
    parser_classes = (MultiPartParser,FormParser)

    def get_object(self, slug):
        try:
            return BlogModel.objects.get(slug=slug)
        except BlogModel.DoesNotExist:
            raise Http404


    def get(self, request, slug, format=None):
        data = self.get_object(slug)
        serilizer = BlogSerializer(data)
        return Response(serilizer.data)

    def put(self,request,slug):
        data = self.get_object(slug)
        serializer = BlogSerializer(data , data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, slug, format=None):
        data = self.get_object(slug)
        data.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
    #permission_classes = [permissions.IsAuthenticatedOrReadOnly]




class EventView(APIView):
    #permission_classes = (IsAuthenticated,SuperAdminPermission)
    #authentication_classes = (SessionAuthentication,)
    parser_classes = (MultiPartParser,FormParser)

    def get(self,request,format = None):
        data = EventModel.objects.all()
        serializer = EventSerializer (data, many = True)
        return Response(serializer.data,status = status.HTTP_200_OK)

    def post(self,request, format=None):
        serializer = EventSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status = status.HTTP_201_CREATED)
        else:
            return Response(serializer.data,status = status.HTTP_400_BAD_REQUEST)
class DetailEventView(APIView):
    #permission_classes = (IsAuthenticated,SuperAdminPermission)
    #authentication_classes = (SessionAuthentication,)
    parser_classes = (MultiPartParser,FormParser)


    def get_object(self, slug):
        try:
            return EventModel.objects.get(id=slug)
        except EventModel.DoesNotExist:
            raise Http404


    def get(self, request, slug, format=None):
        data = self.get_object(slug)
        serilizer = EventSerializer(data)
        return Response(serilizer.data)

    def put(self,request,slug):
        data = self.get_object(slug)
        serializer = EventSerializer(data , data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, slug, format=None):
        data = self.get_object(slug)
        data.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)




class CallobrationsView(APIView):
    #permission_classes = (IsAuthenticated,SuperAdminPermission)
    #authentication_classes = (SessionAuthentication,)
    parser_classes = (MultiPartParser,FormParser)

    def get(self,request,format = None):
        data = CallobrationsModel.objects.all()
        serializer = CallobrationsSerializer (data, many = True)
        return Response(serializer.data,status = status.HTTP_200_OK)

    def post(self,request, format=None):
        serializer = CallobrationsSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status = status.HTTP_201_CREATED)
        else:
            return Response(serializer.data,status = status.HTTP_400_BAD_REQUEST)
class DetailCallobrationsView(APIView):
    #permission_classes = (IsAuthenticated,SuperAdminPermission)
    #authentication_classes = (SessionAuthentication,)
    parser_classes = (MultiPartParser,FormParser)


    def get_object(self, slug):
        try:
            return CallobrationsModel.objects.get(id=slug)
        except CallobrationsModel.DoesNotExist:
            raise Http404


    def get(self, request, slug, format=None):
        data = self.get_object(slug)
        serilizer = CallobrationsSerializer(data)
        return Response(serilizer.data)

    def put(self,request,slug):
        data = self.get_object(slug)
        serializer = CallobrationsSerializer(data , data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, slug, format=None):
        data = self.get_object(slug)
        data.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class OnlineTestView(APIView):
    #permission_classes = (IsAuthenticated,SuperAdminPermission)
    #authentication_classes = (SessionAuthentication,)
    parser_classes = (MultiPartParser,FormParser)

    def get(self,request,format = None):
        data = OnlineTestModel.objects.all()
        serializer = OnlineTestSerializer(data, many = True)
        return Response(serializer.data,status = status.HTTP_200_OK)

    def post(self,request, format=None):
        serializer = OnlineTestSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status = status.HTTP_201_CREATED)
        else:
            return Response(serializer.data,status = status.HTTP_400_BAD_REQUEST)
class DetailOnlineTestView(APIView):
    parser_classes = (MultiPartParser,FormParser)


    def get_object(self, pk):
        try:
            return OnlineTestModel.objects.get(id=pk)
        except OnlineTestModel.DoesNotExist:
            raise Http404


    def get(self, request, pk, format=None):
        data = self.get_object(pk)
        serilizer = OnlineTestSerializer(data)
        return Response(serilizer.data)

    def put(self,request,pk):
        data = self.get_object(pk)
        serializer = OnlineTestSerializer(data , data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        data = self.get_object(pk)
        data.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
class CoursesView(APIView):
    parser_classes = (MultiPartParser,FormParser)
    def get(self,request,format = None):
        data = CourseModel.objects.all()
        serializer = CourseSerializer(data, many = True,context={'request': request})
        return Response(serializer.data,status = status.HTTP_200_OK)

    def post(self,request, format=None):
        serializer = CourseSerializer(data=request.data,context={'request': request})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status = status.HTTP_201_CREATED)
        else:
            return Response(serializer.data,status = status.HTTP_400_BAD_REQUEST)

    def perform_create(self, serializer):
        serializer.save(User=self.request.user)

class DetailCoursesView(APIView):
    parser_classes = (MultiPartParser,FormParser)

    def get_object(self, slug):
        try:
            return CourseModel.objects.get(slug=slug)
        except CourseModel.DoesNotExist:
            raise Http404


    def get(self, request, slug, format=None):
        data = self.get_object(slug)
        serilizer = CourseSerializer(data,context={'request': request})
        return Response(serilizer.data)

    def put(self,request,slug):
        data = self.get_object(slug)
        serializer = CourseSerializer(data , data=request.data,context={'request': request})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, slug, format=None):
        data = self.get_object(slug)
        data.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]

class ComplaintView(APIView):
    parser_classes = (MultiPartParser,FormParser)
    def get(self,request,format = None):
        data = ComplaintModel.objects.all()
        serializer = ComplaintSerializer (data, many = True)
        return Response(serializer.data,status = status.HTTP_200_OK)

    def post(self,request, format=None):
        serializer = ComplaintSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status = status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors,status = status.HTTP_400_BAD_REQUEST)

class DetailComplaintView(APIView):
    permission_classes = (IsAuthenticated,SuperAdminPermission)
    authentication_classes = (SessionAuthentication,)
    parser_classes = (MultiPartParser,FormParser)
    def get_object(self, pk):
        try:
            return ComplaintModel.objects.get(id=pk)
        except ComplaintModel.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        data = self.get_object(pk)
        serilizer = ComplaintSerializer(data)
        return Response(serilizer.data)

    def put(self,request,pk):
        data = self.get_object(pk)
        serializer = ComplaintSerializer(data , data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        data = self.get_object(pk)
        data.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class ScheduleView(APIView):
    permission_classes = (IsAuthenticated,SuperAdminPermission)
    authentication_classes = (SessionAuthentication,)
    def get(self,request,format = None):
        data = ScheduleModel.objects.all()
        serializer = ScheduleSerializer (data, many = True)
        return Response(serializer.data,status = status.HTTP_200_OK)

    def post(self,request, format=None):
        serializer = ScheduleSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status = status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors,status = status.HTTP_400_BAD_REQUEST)

class DetailScheduleView(APIView):
    permission_classes = (IsAuthenticated,SuperAdminPermission)
    authentication_classes = (SessionAuthentication,)
    parser_classes = (MultiPartParser,FormParser)
    def get_object(self, pk):
        try:
            return ScheduleModel.objects.get(id=pk)
        except ScheduleModel.DoesNotExist:
            raise Http404


    def get(self, request, pk, format=None):
        data = self.get_object(pk)
        serilizer = ScheduleSerializer(data)
        return Response(serilizer.data)

    def put(self,request,pk):
        data = self.get_object(pk)
        serializer = ScheduleSerializer(data , data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        data = self.get_object(pk)
        data.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class AttendanceView(APIView):
    #permission_classes = (IsAuthenticated,SuperAdminPermission)
    #authentication_classes = (SessionAuthentication,)
    parser_classes = (MultiPartParser,FormParser)
    def get(self,request,format = None):
        data = AttendanceModel.objects.all()
        serializer = AttendanceSerializer (data, many = True)
        return Response(serializer.data,status = status.HTTP_200_OK)

    def post(self,request, format=None):
        serializer = AttendanceSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status = status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors,status = status.HTTP_400_BAD_REQUEST)

class DetailAttendanceView(APIView):
    #permission_classes = (IsAuthenticated,SuperAdminPermission)
    #authentication_classes = (SessionAuthentication,)
    parser_classes = (MultiPartParser,FormParser)
    def get_object(self, pk):
        try:
            return AttendanceModel.objects.get(id=pk)
        except AttendanceModel.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        data = self.get_object(pk)
        serilizer = AttendanceSerializer(data)
        return Response(serilizer.data)

    def put(self,request,pk):
        data = self.get_object(pk)
        serializer = AttendanceSerializer(data , data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        data = self.get_object(pk)
        data.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class KidsStoryView(APIView):
    parser_classes = (MultiPartParser,FormParser)
    #permission_class = (permissions.IsAuthenticatedOrReadOnly)
    def get(self,request,format = None):
        data = KidStoryModel.objects.all()
        serializer = KidStorySerializer (data, many = True)
        return Response(serializer.data,status = status.HTTP_200_OK)

    def post(self,request, format=None):
        serializer = KidStorySerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status = status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors,status = status.HTTP_400_BAD_REQUEST)


    def perform_create(self, serializer):
        serializer.save(User=self.request.user)


class DetailKidsStoryView(APIView):
    parser_classes = (MultiPartParser,FormParser)

    def get_object(self, slug):
        try:
            return KidStoryModel.objects.get(slug=slug)
        except KidStoryModel.DoesNotExist:
            raise Http404


    def get(self, request, slug, format=None):
        data = self.get_object(slug)
        serilizer = KidStorySerializer(data)
        return Response(serilizer.data)

    def put(self,request,slug):
        data = self.get_object(slug)
        serializer = KidStorySerializer(data , data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, slug, format=None):
        data = self.get_object(slug)
        data.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
    #permission_classes = [permissions.IsAuthenticatedOrReadOnly]

class KidsTalentView(APIView):
    parser_classes = (MultiPartParser,FormParser)
    #permission_class = (permissions.IsAuthenticatedOrReadOnly)
    def get(self,request,format = None):
        data = KidTalentModel.objects.all()
        serializer = KidTalentSerializer (data, many = True)
        return Response(serializer.data,status = status.HTTP_200_OK)

    def post(self,request, format=None):
        serializer = KidTalentSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status = status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors,status = status.HTTP_400_BAD_REQUEST)


    def perform_create(self, serializer):
        serializer.save(User=self.request.user)


class DetailKidsTalentView(APIView):
    parser_classes = (MultiPartParser,FormParser)

    def get_object(self, slug):
        try:
            return KidTalentModel.objects.get(slug=slug)
        except KidTalentModel.DoesNotExist:
            raise Http404


    def get(self, request, slug, format=None):
        data = self.get_object(slug)
        serilizer = KidTalentSerializer(data)
        return Response(serilizer.data)

    def put(self,request,slug):
        data = self.get_object(slug)
        serializer = KidTalentSerializer(data , data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, slug, format=None):
        data = self.get_object(slug)
        data.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
    #permission_classes = [permissions.IsAuthenticatedOrReadOnly]

class QuizContextsView(APIView):
    parser_classes = (MultiPartParser,FormParser)
    #permission_class = (permissions.IsAuthenticatedOrReadOnly)
    def get(self,request,format = None):
        data = QuizContextModel.objects.all()
        serializer = QuizContextSerializer (data, many = True)
        return Response(serializer.data,status = status.HTTP_200_OK)

    def post(self,request, format=None):
        serializer = QuizContextSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status = status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors,status = status.HTTP_400_BAD_REQUEST)


    def perform_create(self, serializer):
        serializer.save(User=self.request.user)


class DetailQuizContextsView(APIView):
    parser_classes = (MultiPartParser,FormParser)

    def get_object(self, slug):
        try:
            return QuizContextModel.objects.get(slug=slug)
        except QuizContextModel.DoesNotExist:
            raise Http404


    def get(self, request, slug, format=None):
        data = self.get_object(slug)
        serilizer = QuizContextSerializer(data)
        return Response(serilizer.data)

    def put(self,request,slug):
        data = self.get_object(slug)
        serializer = QuizContextSerializer(data , data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, slug, format=None):
        data = self.get_object(slug)
        data.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
    #permission_classes = [permissions.IsAuthenticatedOrReadOnly]

class ApprovalsView(APIView):
    parser_classes = (MultiPartParser,FormParser)
    #permission_class = (permissions.IsAuthenticatedOrReadOnly)
    def get(self,request,format = None):
        data = ApprovalModel.objects.all()
        serializer = ApprovalSerializer (data, many = True)
        return Response(serializer.data,status = status.HTTP_200_OK)

    def post(self,request, format=None):
        serializer = ApprovalSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status = status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors,status = status.HTTP_400_BAD_REQUEST)


    def perform_create(self, serializer):
        serializer.save(User=self.request.user)


class DetailApprovalsView(APIView):
    parser_classes = (MultiPartParser,FormParser)

    def get_object(self, slug):
        try:
            return ApprovalModel.objects.get(slug=slug)
        except ApprovalModel.DoesNotExist:
            raise Http404


    def get(self, request, slug, format=None):
        data = self.get_object(slug)
        serilizer = ApprovalSerializer(data)
        return Response(serilizer.data)

    def put(self,request,slug):
        data = self.get_object(slug)
        serializer = ApprovalSerializer(data , data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, slug, format=None):
        data = self.get_object(slug)
        data.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
    #permission_classes = [permissions.IsAuthenticatedOrReadOnly]


class BusinessOffersView(APIView):
    parser_classes = (MultiPartParser,FormParser)
    #permission_class = (permissions.IsAuthenticatedOrReadOnly)
    def get(self,request,format = None):
        data = BusinessOfferModel.objects.all()
        serializer = BusinessOfferSerializer (data, many = True)
        return Response(serializer.data,status = status.HTTP_200_OK)

    def post(self,request, format=None):
        serializer = BusinessOfferSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status = status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors,status = status.HTTP_400_BAD_REQUEST)


    def perform_create(self, serializer):
        serializer.save(User=self.request.user)


class DetailBusinessOffersView(APIView):
    parser_classes = (MultiPartParser,FormParser)

    def get_object(self, slug):
        try:
            return BusinessOfferModel.objects.get(slug=slug)
        except BusinessOfferModel.DoesNotExist:
            raise Http404


    def get(self, request, slug, format=None):
        data = self.get_object(slug)
        serilizer = BusinessOfferSerializer(data)
        return Response(serilizer.data)

    def put(self,request,slug):
        data = self.get_object(slug)
        serializer = BusinessOfferSerializer(data , data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, slug, format=None):
        data = self.get_object(slug)
        data.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
    #permission_classes = [permissions.IsAuthenticatedOrReadOnly]

class DepartmentsView(APIView):
    parser_classes = (MultiPartParser,FormParser)
    #permission_class = (permissions.IsAuthenticatedOrReadOnly)
    def get(self,request,format = None):
        data = DepartmentModel.objects.all()
        serializer = DepartmentSerializer (data, many = True)
        return Response(serializer.data,status = status.HTTP_200_OK)

    def post(self,request, format=None):
        serializer = DepartmentSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status = status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors,status = status.HTTP_400_BAD_REQUEST)


    def perform_create(self, serializer):
        serializer.save(User=self.request.user)


class DetailDepartmentsView(APIView):
    parser_classes = (MultiPartParser,FormParser)

    def get_object(self, slug):
        try:
            return DepartmentModel.objects.get(slug=slug)
        except DepartmentModel.DoesNotExist:
            raise Http404


    def get(self, request, slug, format=None):
        data = self.get_object(slug)
        serilizer = DepartmentSerializer(data)
        return Response(serilizer.data)

    def put(self,request,slug):
        data = self.get_object(slug)
        serializer = DepartmentSerializer(data , data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, slug, format=None):
        data = self.get_object(slug)
        data.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
    #permission_classes = [permissions.IsAuthenticatedOrReadOnly]

class TeamsView(APIView):
    parser_classes = (MultiPartParser,FormParser)
    #permission_class = (permissions.IsAuthenticatedOrReadOnly)
    def get(self,request,format = None):
        data = TeamModel.objects.all()
        serializer = TeamSerializer (data, many = True)
        return Response(serializer.data,status = status.HTTP_200_OK)

    def post(self,request, format=None):
        serializer = TeamSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status = status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors,status = status.HTTP_400_BAD_REQUEST)


    def perform_create(self, serializer):
        serializer.save(User=self.request.user)


class DetailTeamsView(APIView):
    parser_classes = (MultiPartParser,FormParser)

    def get_object(self, slug):
        try:
            return TeamModel.objects.get(slug=slug)
        except TeamModel.DoesNotExist:
            raise Http404


    def get(self, request, slug, format=None):
        data = self.get_object(slug)
        serilizer = TeamSerializer(data)
        return Response(serilizer.data)

    def put(self,request,slug):
        data = self.get_object(slug)
        serializer = TeamSerializer(data , data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, slug, format=None):
        data = self.get_object(slug)
        data.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
    #permission_classes = [permissions.IsAuthenticatedOrReadOnly]

class AdvisoryBoardsView(APIView):
    parser_classes = (MultiPartParser,FormParser)
    #permission_class = (permissions.IsAuthenticatedOrReadOnly)
    def get(self,request,format = None):
        data = AdvisoryBoardModel.objects.all()
        serializer = AdvisoryBoardSerializer (data, many = True)
        return Response(serializer.data,status = status.HTTP_200_OK)

    def post(self,request, format=None):
        serializer = AdvisoryBoardSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status = status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors,status = status.HTTP_400_BAD_REQUEST)


    def perform_create(self, serializer):
        serializer.save(User=self.request.user)


class DetailAdvisoryBoardsView(APIView):
    parser_classes = (MultiPartParser,FormParser)

    def get_object(self, slug):
        try:
            return AdvisoryBoardModel.objects.get(slug=slug)
        except AdvisoryBoardModel.DoesNotExist:
            raise Http404


    def get(self, request, slug, format=None):
        data = self.get_object(slug)
        serilizer = AdvisoryBoardSerializer(data)
        return Response(serilizer.data)

    def put(self,request,slug):
        data = self.get_object(slug)
        serializer = AdvisoryBoardSerializer(data , data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, slug, format=None):
        data = self.get_object(slug)
        data.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
    #permission_classes = [permissions.IsAuthenticatedOrReadOnly]

class FeedbacksView(APIView):
    parser_classes = (MultiPartParser,FormParser)
    #permission_class = (permissions.IsAuthenticatedOrReadOnly)
    def get(self,request,format = None):
        data = FeedbackModel.objects.all()
        serializer = FeedbackSerializer (data, many = True)
        return Response(serializer.data,status = status.HTTP_200_OK)

    def post(self,request, format=None):
        serializer = FeedbackSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status = status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors,status = status.HTTP_400_BAD_REQUEST)


    def perform_create(self, serializer):
        serializer.save(User=self.request.user)


class DetailFeedbacksView(APIView):
    parser_classes = (MultiPartParser,FormParser)

    def get_object(self, slug):
        try:
            return FeedbackModel.objects.get(slug=slug)
        except FeedbackModel.DoesNotExist:
            raise Http404


    def get(self, request, slug, format=None):
        data = self.get_object(slug)
        serilizer = FeedbackSerializer(data)
        return Response(serilizer.data)

    def put(self,request,slug):
        data = self.get_object(slug)
        serializer = FeedbackSerializer(data , data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, slug, format=None):
        data = self.get_object(slug)
        data.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
    #permission_classes = [permissions.IsAuthenticatedOrReadOnly]

class WebsiteAdsView(APIView):
    parser_classes = (MultiPartParser,FormParser)
    #permission_class = (permissions.IsAuthenticatedOrReadOnly)
    def get(self,request,format = None):
        data = WebsiteAdModel.objects.all()
        serializer = WebsiteAdSerializer (data, many = True)
        return Response(serializer.data,status = status.HTTP_200_OK)

    def post(self,request, format=None):
        serializer = WebsiteAdSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status = status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors,status = status.HTTP_400_BAD_REQUEST)


    def perform_create(self, serializer):
        serializer.save(User=self.request.user)


class DetailWebsiteAdsView(APIView):
    parser_classes = (MultiPartParser,FormParser)

    def get_object(self, slug):
        try:
            return WebsiteAdModel.objects.get(slug=slug)
        except WebsiteAdModel.DoesNotExist:
            raise Http404


    def get(self, request, slug, format=None):
        data = self.get_object(slug)
        serilizer = WebsiteAdSerializer(data)
        return Response(serilizer.data)

    def put(self,request,slug):
        data = self.get_object(slug)
        serializer = WebsiteAdSerializer(data , data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, slug, format=None):
        data = self.get_object(slug)
        data.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
    #permission_classes = [permissions.IsAuthenticatedOrReadOnly]

class QueriesView(APIView):
    parser_classes = (MultiPartParser,FormParser)
    #permission_class = (permissions.IsAuthenticatedOrReadOnly)
    def get(self,request,format = None):
        data = QueriesModel.objects.all()
        serializer = QueriesSerializer (data, many = True)
        return Response(serializer.data,status = status.HTTP_200_OK)

    def post(self,request, format=None):
        serializer = QueriesSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status = status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors,status = status.HTTP_400_BAD_REQUEST)


    def perform_create(self, serializer):
        serializer.save(User=self.request.user)


class DetailQueriesView(APIView):
    parser_classes = (MultiPartParser,FormParser)

    def get_object(self, slug):
        try:
            return QueriesModel.objects.get(slug=slug)
        except QueriesModel.DoesNotExist:
            raise Http404


    def get(self, request, slug, format=None):
        data = self.get_object(slug)
        serilizer = QueriesSerializer(data)
        return Response(serilizer.data)

    def put(self,request,slug):
        data = self.get_object(slug)
        serializer = QueriesSerializer(data , data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, slug, format=None):
        data = self.get_object(slug)
        data.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
    #permission_classes = [permissions.IsAuthenticatedOrReadOnly]
