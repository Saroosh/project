from django.contrib import admin
from django.urls import path
from superadmin.views import *
from superadmin import views
from django.conf.urls import url, include
from django.conf import settings
from django.conf.urls.static import static
from django.views.static import serve
from rest_framework.urlpatterns import format_suffix_patterns


urlpatterns = [

    # path('forget', views.forget, name = "forget"),
    # path('reset', views.reset, name = "reset"),
    # path('portal', views.portal , name = "portal"),
    # path('', views.home, name = "home"),
    # path('log_in', views.login_page , name = "login_page"),
    # path('log_out', views.logout_page , name = "logout_page"),
    # path('Assignment', views.assignment, name = "assignmnet_page"),
    path('course/', views.CoursesView.as_view(), name = "course"),
    path('course_detail/<int:pk>/', views.DetailCoursesView.as_view(), name = "course_detail"),

    path('content/', views.ContentsView.as_view() , name = "content"),
    path('content/<str:slug>/', views.DetailContentsView.as_view() , name = "content_detail"),

    path('option/', views.OptionsView.as_view() , name = "option"),
    path('option/<str:slug>/', views.DetailOptionsView.as_view() , name = "option_detail"),

    path('question/', views.QuestionsView.as_view() , name = "question"),
    path('question/<str:slug>/', views.DetailQuestionsView.as_view() , name = "question_detail"),


    path('onlinetest/', views.OnlineTestView.as_view(), name = "onlinetest"),
    path('onlinetest_detail/<int:pk>/', views.DetailOnlineTestView.as_view(), name = "onlinetest"),

    path('article/', views.ArticlesView.as_view() , name = "article"),
    path('article/<str:slug>/', views.DetailArticlesView.as_view() , name = "article_detail"),

    path('school/', views.SchoolsView.as_view() , name = "school"),
    path('school/<str:slug>/', views.DetailSchoolsView.as_view() , name = "school_detail"),

    path('blog/', views.BlogsView.as_view() , name = "blog"),
    path('blog/<str:slug>/', views.DetailBlogsView.as_view() , name = "blog_detail"),

    path('complaint', views.ComplaintView.as_view(), name = "complaint"),
    path('complaint_detail/<int:pk>/', views.DetailComplaintView.as_view(), name = "complaint"),

    path('event/', views.EventView.as_view() , name = "event"),
    path('event/<str:slug>/', views.DetailEventView.as_view() , name = "event_detail"),

    path('users/', views.UserListView.as_view()),
    path('users/<int:pk>/', views.UserDetalView.as_view()),

    path('profile/', views.ProfileView.as_view()),

    path('callaborations/', views.CallobrationsView.as_view() , name = "callaboration"),
    path('callaborations/<str:slug>/', views.DetailCallobrationsView.as_view() , name = "callaboration_detail"),

    path('KidStory/', views.KidsStoryView.as_view() , name = "KidStory"),
    path('KidStory/<str:slug>/', views.DetailKidsStoryView.as_view() , name = "KidStory_detail"),

    path('Attendance/', views.AttendanceView.as_view() , name = "Attendance"),
    path('Attendance/<str:slug>/', views.DetailAttendanceView.as_view() , name = "Attendance_detail"),


    path('KidTalent/', views. KidsTalentView.as_view() , name = "KidTalent"),
    path('KidTalent/<str:slug>/', views.DetailKidsTalentView.as_view() , name = "KidTalent_detail"),

    path('QuizContex/', views.QuizContextsView.as_view() , name = "QuizContex"),
    path('QuizContex/<str:slug>/', views.DetailQuizContextsView.as_view() , name = "QuizContex_detail"),

    path('Approval/', views.ApprovalsView.as_view() , name = "Approval"),
    path('Approval/<str:slug>/', views.DetailApprovalsView.as_view() , name = "Approval_detail"),

    path('BusinessOffer/', views.BusinessOffersView.as_view() , name = "BusinessOffer"),
    path('BusinessOffer/<str:slug>/', views.DetailBusinessOffersView.as_view() , name = "BusinessOffer_detail"),

    path('Department/', views.DepartmentsView.as_view() , name = "Department"),
    path('Department/<str:slug>/', views.DetailDepartmentsView.as_view() , name = "Department_detail"),

    path('Complaint/', views.ComplaintView.as_view() , name = "Complaint"),
    path('Complaint/<str:slug>/', views.DetailComplaintView.as_view() , name = "Complaint_detail"),


    path('Teams/', views.TeamsView.as_view() , name = "Teams"),
    path('Teams/<str:slug>/', views.DetailTeamsView.as_view() , name = "Teams_detail"),

    path('AdvisoryBoards/', views.AdvisoryBoardsView.as_view() , name = "AdvisoryBoards"),
    path('AdvisoryBoards/<str:slug>/', views.DetailAdvisoryBoardsView.as_view() , name = "AdvisoryBoards_detail"),

    path('Feedback/', views.FeedbacksView.as_view() , name = "Feedback"),
    path('Feedback/<str:slug>/', views.DetailFeedbacksView.as_view() , name = "Feedback_detail"),

    path('WebsiteAd/', views.WebsiteAdsView.as_view() , name = "WebsiteAd"),
    path('WebsiteAd/<str:slug>/', views.DetailWebsiteAdsView.as_view() , name = "WebsiteAd_detail"),



]
