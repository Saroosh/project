
# Create your views here.
from django.shortcuts import render
from django.contrib.auth.models import User
from django.contrib.auth import logout, authenticate, login

# ***************** API ****************
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser
from rest_framework.views import APIView
from rest_framework.parsers import JSONParser,FileUploadParser,MultiPartParser,FormParser
from rest_framework.response import Response
from rest_framework import status,viewsets
from rest_framework.permissions import IsAuthenticated,IsAuthenticatedOrReadOnly, AllowAny

# models
from teacherApp.models import *
from adminapp.models import TblClassModel

# serializers
from Student.serializers import AssignmentSerializer
from teacherApp.serializers import *

# Create your views here.
class ShowAssignment(APIView):
    # permission_classes = (IsAuthenticated,SuperAdminOrTeacherPermission)
    # authentication_classes = (SessionAuthentication,)
    # def get_object(self):
    #     try:
    #         return AssignmentModel.objects.all()
    #     except AssignmentModel.DoesNotExist:
    #         return Response(status = status.HTTP_404_NOT_FOUND)

    def get(self, request,slug,format=None):
        try:
            Class = TblClassModel.objects.get(slug = slug)
            data = AssignmentModel.objects.filter(classes = Class)
            serilizer = AssignmentSerializer(data, many = True)
            return Response(serilizer.data)
        except:
            return Response(status = status.HTTP_404_NOT_FOUND)