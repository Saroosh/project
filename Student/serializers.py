from rest_framework import serializers
from .models import *
from teacherApp.models import AssignmentModel

class AssignmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = AssignmentModel
        fields = '__all__'